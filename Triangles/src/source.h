#ifndef DEMO_H
#define DEMO_H

#include <QMainWindow>
#include <QGLWidget>
#include <QGridLayout>
#include <QFrame>
#include <vector>
#include <iostream>
#include <algorithm>

#if _MSC_VER
    #include <gl/glu.h>
#elif __APPLE__
  #include <OpenGL/glu.h>
#else
    #include <GL/glu.h>
#endif



#include "vecmath.h"

class CGView;

class CGMainWindow : public QMainWindow {
	Q_OBJECT

public:

    CGMainWindow (QWidget* parent = 0, Qt::WindowFlags flags = Qt::Window);
	~CGMainWindow ();
    static CGMainWindow* ptr;

protected:

	void keyPressEvent(QKeyEvent*);

private:

	CGView *ogl;

};

class Triangle{
public:
    Vector3d points[3];

    Triangle();

    void draw(bool intersect=false);

    bool intersects(const Triangle& b) const;
    bool getIntersection(const Triangle& b, Vector3d inter[2]) const;
};

class CGView : public QGLWidget {
	Q_OBJECT

public:

    CGView(QWidget*);
	void initializeGL();

    double zoom;
    Vector3d center;
    double phi,theta;
    int buttonState;

    Triangle t1;
    Triangle t2;

    Vector3d* selected;
	
protected:

	void paintGL();
    void drawTriangles();
	void resizeGL(int,int);
	void mouseToTrackball(int x, int y, int W, int H, Vector3d &v);
	Quat4d trackball(const Vector3d&, const Vector3d&);

	void mouseMoveEvent(QMouseEvent*);
	void mousePressEvent(QMouseEvent*);
	void mouseReleaseEvent(QMouseEvent*);
	void wheelEvent(QWheelEvent*);

	Quat4d q_now;
    	int oldX,oldY;

};

#endif
