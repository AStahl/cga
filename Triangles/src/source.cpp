#include <QApplication>
#include <QMenu>
#include <QMenuBar>
#include <QStatusBar>
#include <QFileDialog>
#include <QGLWidget>
#include <QKeyEvent>
#include <QMessageBox>
#include <QTextEdit>
#include <QHBoxLayout>
#include "source.h"

#include <cmath>
#include <fstream>
#include <sstream>
#include <iostream>
#include <limits>
#include <stdlib.h>

#ifdef max
#undef max
#endif

using namespace std;

CGMainWindow* CGMainWindow::ptr;

CGMainWindow::CGMainWindow (QWidget* parent, Qt::WindowFlags flags)
				: QMainWindow (parent, flags) {
                                ptr = this;
								resize (1024, 768);
								setWindowState(Qt::WindowMaximized);

								// Create a menu
                                QMenu *file = new QMenu("&File",this);
								file->addAction ("Quit", qApp, SLOT(quit()), Qt::CTRL+Qt::Key_Q);
								menuBar()->addMenu(file);

								// Create a nice frame to put around the OpenGL widget
								QFrame* f = new QFrame (this);
								f->setFrameStyle(QFrame::Sunken | QFrame::Panel);
								f->setLineWidth(2);

								// Create our OpenGL widget
                                ogl = new CGView (f);

								// Put the GL widget inside the frame
								QHBoxLayout* layout = new QHBoxLayout();
								layout->addWidget(ogl);
								layout->setMargin(0);
								f->setLayout(layout);

								setCentralWidget(f);

								statusBar()->showMessage("Ready",1000);
				}

CGMainWindow::~CGMainWindow () {}

Triangle::Triangle(){
    for(int i=0;i<3;++i){
        for(int j=0;j<3;++j){
            points[i][j]=(rand()%100-50.0)/100;
        }
    }
}

void Triangle::draw(bool intersect){
    glColor3d(0,.5,0);
    if(intersect)
        glColor3d(.5,0,0);

    glLineWidth(3.0);
    glPushAttrib(GL_LIGHTING);
    glDisable(GL_LIGHTING);

    glBegin(GL_LINE_LOOP);
    glVertex3dv(points[0].ptr());
    glVertex3dv(points[1].ptr());
    glVertex3dv(points[2].ptr());
    glEnd();

    glPopAttrib();

}

bool Triangle::intersects(const Triangle& b) const{
    Vector3d inter[2];
    return getIntersection(b,inter);
}

bool Triangle::getIntersection(const Triangle& b,Vector3d inter[2]) const{
    //cout << "getInter()\n";

    inter[0]=Vector3d(0,0,0);
    inter[1]=Vector3d(0,0,0);

    Vector3d na, nb; //Ebenennormalen
    na.cross(points[1]-points[0],points[2]-points[0]);
    na.normalize();
    nb.cross(b.points[1]-b.points[0],b.points[2]-b.points[0]);
    nb.normalize();

    //cout<<"Ebenen done:"<<na<<nb<<"\n";

    //parallel
    if(na==nb)
        return false;

    Vector3d hitA[2], hitB[2]; //where do Borders hit other Triangles plane
    int nHitA=0,nHitB=0;

    for(int i=0;i<3;++i){
        Vector3d r=points[(i+1)%3]-points[i];
        if(r*nb!=0){
            double a=-((points[i]-b.points[0])*nb)/(r*nb);
            if(a>=0&&a<1){
                hitA[nHitA]=points[i]+r*a;
                //cout<< "HitA: "<<i<<": "<<a<<" _ "<<hitA[nHitA]<<endl;
                ++nHitA;
            }
        }
    }

    for(int i=0;i<3;++i){
        Vector3d r=b.points[(i+1)%3]-b.points[i];
        if(r*na!=0){
            double a=-((b.points[i]-points[0])*na)/(r*na);
            if(a>=0&&a<1){
                hitB[nHitB]=b.points[i]+r*a;
                //cout<< "HitB: "<<i<<": "<<a<<" _ "<<hitB[nHitB]<<endl;
                ++nHitB;
            }
        }
    }

    //cout<<"Hits calculated: A hits "<<nHitA<<" times:\n";
    //cout<<"B hits "<<nHitB<< "times\n";

    if(nHitA>3||nHitB>3){
        cout << "oops - set breakpoint here";
    }

    if(nHitA!=2||nHitB!=2)
        return false;

    //cout<<hitA[0][0]<<", "<<hitA[0][1]<<", "<<hitA[0][2]<<"\n";
    //cout<<hitA[1][0]<<", "<<hitA[1][1]<<", "<<hitA[1][2]<<"\n";
    //cout<<hitB[0][0]<<", "<<hitB[0][1]<<", "<<hitB[0][2]<<"\n";
    //cout<<hitB[1][0]<<", "<<hitB[1][1]<<", "<<hitB[1][2]<<"\n";

    Vector3d dI=hitA[0]-hitA[1];  //direction of the Intersection, direction/chosen points doesnt matter.
    dI.normalize();

    //cout<<"Intersection-Vector: "<<dI[0]<<", "<<dI[1]<<", "<<dI[2]<<"\n";

    //get 2 Intervals along the Intersection
    double dA[2],dB[2];
    dA[0]=hitA[0]*dI;
    dA[1]=hitA[1]*dI;
    dB[0]=hitB[0]*dI;
    dB[1]=hitB[1]*dI;

    //cout<<"gotIntervals: "<<dA[0]<<", "<<dA[1]<<", "<<dB[0]<<", "<<dB[1]<<"\n";

    //compare intervals
    if(std::min(dA[0],dA[1])>=std::max(dB[0],dB[1])||std::min(dB[0],dB[1])>=std::max(dA[0],dA[1]))
        return false;

    double d[2];

    //get intersection
    d[0]=std::max(std::min(dA[0],dA[1]),std::min(dB[0],dB[1]));
    d[1]=std::min(std::max(dA[0],dA[1]),std::max(dB[0],dB[1]));

    inter[0]=hitA[0]+dI*(d[0]-dA[0]);
    inter[1]=hitA[0]+dI*(d[1]-dA[0]);

    //cout<<"COLLISION!\n";

    return true;
}


void CGMainWindow::keyPressEvent(QKeyEvent* event) {
    if(event->key()==Qt::Key_1){
        ogl->selected=ogl->t1.points;
    }
    if(event->key()==Qt::Key_2){
        ogl->selected=ogl->t1.points+1;
    }
    if(event->key()==Qt::Key_3){
        ogl->selected=ogl->t1.points+2;
    }
    if(event->key()==Qt::Key_4){
        ogl->selected=ogl->t2.points;
    }
    if(event->key()==Qt::Key_5){
        ogl->selected=ogl->t2.points+1;
    }
    if(event->key()==Qt::Key_6){
        ogl->selected=ogl->t2.points+2;
    }
    if(event->key()==Qt::Key_A){
        ogl->selected->operator [](0)-=0.005;
    }
    if(event->key()==Qt::Key_D){
        ogl->selected->operator [](0)+=0.005;
    }
    if(event->key()==Qt::Key_S){
        ogl->selected->operator [](1)-=0.005;
    }
    if(event->key()==Qt::Key_W){
        ogl->selected->operator [](1)+=0.005;
    }
    if(event->key()==Qt::Key_Q){
        ogl->selected->operator [](2)-=0.005;
    }
    if(event->key()==Qt::Key_E){
        ogl->selected->operator [](2)+=0.005;
    }
    ogl->updateGL();
}


CGView::CGView (QWidget* parent ) : QGLWidget (parent) {
    selected=t1.points;
}



void CGView::drawTriangles(){

    Vector3d inter[2];
    bool intersect = t1.getIntersection(t2,inter);

    glLineWidth(2.0);
    glPushAttrib(GL_LIGHTING);
    glDisable(GL_LIGHTING);

    //coordinates
    glColor3d(1,0,0);
    glBegin(GL_LINES);
    glVertex3d(0,0,0);
    glVertex3d(1,0,0);
    glEnd();
    glColor3d(0,1,0);
    glBegin(GL_LINES);
    glVertex3d(0,0,0);
    glVertex3d(0,1,0);
    glEnd();
    glColor3d(0,0,1);
    glBegin(GL_LINES);
    glVertex3d(0,0,0);
    glVertex3d(0,0,1);
    glEnd();

    //selected
    glPushMatrix();
    glTranslated((*selected)[0],(*selected)[1],(*selected)[2]);
    glColor3d(0,0,0);
    glBegin(GL_LINE_LOOP);
    glVertex3d(0.02,0,0);
    glVertex3d(0,0.02,0);
    glVertex3d(-0.02,0,0);
    glVertex3d(0,-0.02,0);
    glEnd();
    glPopMatrix();

    //Intersection
    if(intersect){
        glColor3d(1,0,0);
        glLineWidth(5.0);
        glBegin(GL_LINES);
        glVertex3dv(inter[0].ptr());
        glVertex3dv(inter[1].ptr());
        glEnd();

    }

    //restore settings
    glPopAttrib();


    t1.draw(intersect);
    t2.draw(intersect);
}

void CGView::initializeGL() {
				qglClearColor(Qt::white);
                zoom = 1.0;
                center = 0.0;
				q_now = Quat4d(0.0,0.0,0.0,1.0);
				glEnable(GL_DEPTH_TEST);
				glEnable(GL_COLOR_MATERIAL);
				glEnable(GL_LIGHT0);
				glEnable(GL_LIGHTING);
				glEnable(GL_BLEND);
				glEnable(GL_NORMALIZE);
				glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
				float pos[4] = {0,2,2,1};
				glLightfv(GL_LIGHT0,GL_POSITION, pos);

}

void CGView::paintGL() {
				glMatrixMode(GL_MODELVIEW);
				glLoadIdentity();
				glTranslatef(0,0,-2);


				Matrix4d R(q_now);
				Matrix4d RT = R.transpose();
				glMultMatrixd(RT.ptr());

                glScaled(zoom,zoom,zoom);
                glTranslated(-center[0],-center[1],-center[2]);

				glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

                drawTriangles();

}

void CGView::resizeGL(int width, int height) {
				glViewport(0,0,width,height);
				glMatrixMode(GL_PROJECTION);
				glLoadIdentity();
				if (width > height) {
								double ratio = width/(double) height;
								//glOrtho(-ratio,ratio,-1.0,1.0,-10.0,10.0);
								//glFrustum(-ratio,ratio,-1.0,1.0,1.0,10.0);
								gluPerspective(60,ratio,0.1,100.0);
								//gluLookAt(0,0,-100,0,0,0,0,1,0);
				}
				else {
								double ratio = height/(double) width;
								//glOrtho(-1.0,1.0,-ratio,ratio,-10.0,10.0);
								//glFrustum(-1.0,1.0,-ratio,ratio,1.0,10.0);
								gluPerspective(60,1/ratio,0.1,100.0);
								//		glFrustum(-1.0,1.0,-ratio,ratio,1.0,100.0);
								//		gluPerspective(fov,1.0/ratio,1.0,10000.0);
								//gluLookAt(0,0,-1,0,0,1,0,1,0);
				}
				glMatrixMode (GL_MODELVIEW);
}


void CGView::mousePressEvent(QMouseEvent *event) {
				oldX = event->x();
				oldY = event->y();
				if( event->button() == Qt::LeftButton )
				{
								buttonState = 0;

				}
				else if( event->button() == Qt::RightButton )
				{
								buttonState = 1;
				}
				else
								buttonState = 2;	

}

void CGView::mouseReleaseEvent(QMouseEvent*) {}

void CGView::wheelEvent(QWheelEvent* event) {
				if (event->delta() < 0) zoom *= 1.2; else zoom *= 1/1.2;
				update();
}

void CGView::mouseMoveEvent(QMouseEvent* event) {

				//if( event->button() == Qt::LeftButton )
				if( buttonState == 0 )	/// rotate camera
				{
								Vector3d p1,p2;

								mouseToTrackball(oldX,oldY,width(),height(),p1);
								mouseToTrackball(event->x(),event->y(),width(),height(),p2);

								Quat4d q = trackball(p1,p2);
								q_now = q * q_now;
								q_now.normalize();
				}
				else if( buttonState == 1 ) /// translate object
				{
								float dx,dy;
								dx = dy = .001f;
								Matrix4d R(q_now);
								Matrix4d RT = R.transpose();

								Vector3d v(0,0,0);
								v[0] = -dx*(event->x() - oldX);
								v[1] =  dy*(event->y() - oldY);
								v = RT*v;

								center += v;
								//cRef -= v;
				}


				oldX = event->x();
				oldY = event->y();

				updateGL();
}



void CGView::mouseToTrackball(int x, int y, int W, int H, Vector3d &v) {
				if (W > H) {
								v[0] = (2.0*x-W)/H;
								v[1] = 1.0-y*2.0/H;
				} else {
								v[0] = (2.0*x-W)/W;
								v[1] = (H-2.0*y)/W;
				}
				double d = v[0]*v[0]+v[1]*v[1];
				if (d > 1.0) {
								v[2] = 0.0;
								v /= sqrt(d);
				} else v[2] = sqrt(1.0-d*d);
}

Quat4d CGView::trackball(const Vector3d& u, const Vector3d& v) {
				Vector3d uxv = u % v;
				Quat4d ret(uxv[0],uxv[1],uxv[2],1.0+u*v);
				ret.normalize();
				return ret;
}


int main (int argc, char **argv) {
				QApplication app(argc, argv);

				if (!QGLFormat::hasOpenGL()) {
								qWarning ("This system has no OpenGL support. Exiting.");
								return 1;
				}

				CGMainWindow *w = new CGMainWindow(NULL);

				w->show();

				return app.exec();
}

