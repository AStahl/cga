#ifndef SHAPEMATCHEDOBJECT_H
#define SHAPEMATCHEDOBJECT_H

#include "vecmath.h"
#include <vector>
#include <iostream>

#if _MSC_VER
    #include <gl/glu.h>
#elif __APPLE__
  #include <OpenGL/glu.h>
#else
    #include <GL/glu.h>
#endif

#include <QtOpenGL>

class ShapeMatchedObject {
public:
    ShapeMatchedObject();
    ShapeMatchedObject(std::vector<Vector3d>& points, std::vector<int> &indices, std::vector<Vector3d>& v,  std::vector<float> &w, float alpha, float beta);
    void makeStep(double dt);

    //Anzahl der Massepunkte
    int n;
    //Positionen, Geschwindigkeiten und Positionen in der Ruhelage der Massepunkte
    std::vector<Vector3d> x, p, v, x_0;
    //externe Kräfte
    std::vector<Vector3d> F_ext;
    //indices der Dreiecke zu Zeichnen
    std::vector<int> indices;

    //inverse Massen der Punkte
    std::vector<float> w,m;
    float gravity;

    //steifheitsparameter (zwischen 0 und 1)
    float alpha, beta;

    //Dämpfungsparameter (zwischen 0 und 1)
    float k;

    //zusätzliche z-Ebene
    float z2=3;

    void draw();

private:

    void shapeMatching(Matrix4d &A, Vector3d &t);
};

#endif // SHAPEMATCHEDOBJECT_H
