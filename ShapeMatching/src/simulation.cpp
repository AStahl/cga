#include <QApplication>
#include <QMenu>
#include <QMenuBar>
#include <QStatusBar>
#include <QFileDialog>
#include <QGLWidget>
#include <QKeyEvent>
#include <QMessageBox>
#include <QHBoxLayout>
#include <QTimer>
#include <QtOpenGL>

#define _USE_MATH_DEFINES
#include <cmath>
#include <fstream>
#include <sstream>
#include <iostream>
#include <limits>

#include "vecmath.h"
#ifndef VECMATH_VERSION
#  error "vecmath-library might be out-of-date. Please use newer version"
#endif

#include "OffReader.h"

#include <time.h>

#include "simulation.h"

const double infinity = 1e20;

const unsigned int TIMESTEP = 10; // in msec
bool moving = false;

//Die Timer-Funktion, die regelm"assig in TIMESTEP Schritten aufgerufen wird
void CGView::timer()
{
    if (moving == true){
        sys.makeStep(TIMESTEP/1000.0);
        //moving=false;
        updateGL();
    }
}

CGMainWindow::CGMainWindow (QWidget* parent, Qt::WindowFlags flags)
: QMainWindow (parent, flags) {
	resize (604, 614);

	// Create a nice frame to put around the OpenGL widget
	QFrame* f = new QFrame (this);
	f->setFrameStyle(QFrame::Sunken | QFrame::Panel);
	f->setLineWidth(2);

	// Create our OpenGL widget
	ogl = new CGView (this,f);

	// Create a menu
	QMenu *file = new QMenu("&File",this);
	file->addAction ("Quit", this, SLOT(close()), Qt::CTRL+Qt::Key_Q);

	menuBar()->addMenu(file);

	// Put the GL widget inside the frame
	QHBoxLayout* layout = new QHBoxLayout();
	layout->addWidget(ogl);
	layout->setMargin(0);
	f->setLayout(layout);

	setCentralWidget(f);

	statusBar()->showMessage("Ready",1000);
}

CGMainWindow::~CGMainWindow () {}

CGView::CGView (CGMainWindow *mainwindow,QWidget* parent ) : QGLWidget (parent), quad(NULL) {
	main = mainwindow;

	/// Um Keyboard-Events durchzulassen
	setFocusPolicy(Qt::StrongFocus);
}

void CGView::initializeGL() {
	glClearColor(1.0,1.0,1.0,1.0);
	q_now = Quat4d(0.0,0.0,0.0,1.0);
    zoom = 1.0;
	
    //glEnable(GL_CULL_FACE);
    //glFrontFace(GL_CCW);
    //glCullFace(GL_BACK);
    glEnable(GL_DEPTH_TEST);

	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_NORMALIZE);
	glEnable(GL_COLOR_MATERIAL);
    glEnable(GL_LINE_SMOOTH);
    glHint(GL_LINE_SMOOTH_HINT, GL_NICEST);

    loadOffModel("/home/alex/CGA/OFF-Models/minion.off");
    //loadOffModel("/home/alex/CGA/OFF-Models/duck_triangulate.off");
    initShapeMatchedObject();

	///Make timer
    QTimer *timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(timer()));
    timer->start(TIMESTEP); /// also 100 mal pro sekunde!
}

void CGView::loadOffModel(std::string filename){
    //laden des off-files
    char *a=new char[filename.size()+1];
    a[filename.size()]=0;
    memcpy(a,filename.c_str(),filename.size());
    indices.clear();
    coords.clear();
    LoadOffFile(a, coords, indices);

    //skaliere das Model, sodass alle Models gleich groß sind
    qreal inf = std::numeric_limits<qreal>::infinity();
    QVector3D min = QVector3D( inf, inf, inf);
    QVector3D max = QVector3D(-inf,-inf,-inf);

    qreal x1,x2,y1,y2,z1,z2;
    x1 = min.x();
    y1 = min.y();
    z1 = min.z();
    x2 = max.x();
    y2 = max.y();
    z2 = max.z();

        for(size_t i=0;i<coords.size();i++) {
            x1 = std::min(x1,coords[i].x());
            x2 = std::max(x2,coords[i].x());
            y1 = std::min(y1,coords[i].y());
            y2 = std::max(y2,coords[i].y());
            z1 = std::min(z1,coords[i].z());
            z2 = std::max(z2,coords[i].z());
        }

    min = QVector3D(x1,y1,z1);
    max = QVector3D(x2,y2,z2);
    QVector3D extent = max - min;
    float scale = 1.5/std::max(std::max(extent.x(),extent.y()),extent.z());
    for(size_t i=0; i< coords.size(); i++)
        coords[i] *= scale;

    int n=coords.size();
    for(int i=0; i<n; i++){
        coords[i] = coords[i] + Vector3d(0.0,0.0,2.0);    //verschiebe den körper in z-Richtung
    }
}

void CGView::initShapeMatchedObject(){
    int n=indices.size();
    std::vector<Vector3d> v = std::vector<Vector3d>(n);
    std::vector<float> w = std::vector<float>(n);
    for(int i=0; i<n; i++){
        v[i]=Vector3d(0.0,0.0,0.0);
        w[i]=1.0;
    }
    sys = ShapeMatchedObject(coords,indices,v,w,0.8,0.0);
}

void CGView::paintGL() {
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	/*	Falls perspektivische Projektion verwendet wird, Szene etwas nach hinten schieben, 
		damit sie in der Sichtpyramide liegt. */
    glTranslated(0.0,0.0,-3.0);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	Matrix4d R(q_now);
	Matrix4d RT = R.transpose();
	glMultMatrixd(RT.ptr());

	double z = 0.1;
	glScaled(z,z,z);
    glScaled(zoom,zoom,zoom);

    glColor3d(1,1,0);
    sys.draw();

    //Zeichen z=0 Ebene
    glColor3d(0,1,0);
    glBegin(GL_QUADS);
    float l = 100;
        glVertex3d(-l,-l,0);
        glVertex3d(-l,l,0);
        glVertex3d(l,l,0);
        glVertex3d(l,-l,0);
    glEnd();

    //Zeichne Koordinatensystem
    glDisable(GL_LIGHTING);
    glPushMatrix();
    glScaled(10,10,10);
    glBegin(GL_LINES);
        glColor3d(1,0,0);
        glVertex3d(0,0,0);
        glVertex3d(1,0,0);
        glColor3d(0,1,0);
        glVertex3d(0,0,0);
        glVertex3d(0,1,0);
        glColor3d(0,0,1);
        glVertex3d(0,0,0);
        glVertex3d(0,0,1);
    glEnd();
    glPopMatrix();
    glEnable(GL_LIGHTING);
}

void CGView::resizeGL(int width, int height) {
	glViewport(0,0,width,height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	if (width > height) {
		double ratio = width/(double) height;
        gluPerspective(45,ratio,1.0,10000.0);
        //glOrtho(-ratio,ratio,-1.0,1.0,-10.0,10.0);
    }
    else {
		double ratio = height/(double) width;
        gluPerspective(45,1.0/ratio,0.01,10000.0);
        //glOrtho(-1.0,1.0,-ratio,ratio,-10.0,10.0);
    }

	glMatrixMode (GL_MODELVIEW);
}

void CGView::worldCoord(int x, int y, int z, Vector3d &v) {
	GLint viewport[4];
	glGetIntegerv(GL_VIEWPORT,viewport);
	GLdouble M[16], P[16];
	glGetDoublev(GL_PROJECTION_MATRIX,P);
	glGetDoublev(GL_MODELVIEW_MATRIX,M);
	gluUnProject(x,viewport[3]-1-y,z,M,P,viewport,&v[0],&v[1],&v[2]);
}

void CGView::mousePressEvent(QMouseEvent *event) {
	oldX = event->x();
	oldY = event->y();
    update();
}

void CGView::mouseReleaseEvent(QMouseEvent*) {}

void CGView::wheelEvent(QWheelEvent* event) {
    if (event->delta() < 0) zoom *= 1.2; else zoom *= 1/1.2;
    update();
}

void CGView::mouseToTrackball(int x, int y, int W, int H, Vector3d &v) {
	if (W > H) {
		v[0] = (2.0*x-W)/H;
		v[1] = 1.0-y*2.0/H;
	} else {
		v[0] = (2.0*x-W)/W;
		v[1] = (H-2.0*y)/W;
	}
	double d = v[0]*v[0]+v[1]*v[1];
	if (d > 1.0) {
		v[2] = 0.0;
		v /= sqrt(d);
	} else v[2] = sqrt(1.0-d*d);
}

Quat4d CGView::trackball(const Vector3d& u, const Vector3d& v) {
	Vector3d uxv = u % v;
	Quat4d ret(uxv[0],uxv[1],uxv[2],1.0+u*v);
	ret.normalize();
	return ret;
}

void CGView::mouseMoveEvent(QMouseEvent* event) {
	Vector3d p1,p2;

	mouseToTrackball(oldX,oldY,width(),height(),p1);
	mouseToTrackball(event->x(),event->y(),width(),height(),p2);
	
	Quat4d q = trackball(p1,p2);
	q_now = q * q_now;
	q_now.normalize();

	oldX = event->x();
	oldY = event->y();

	updateGL();
}

void CGView::keyPressEvent( QKeyEvent * event) 
{
	switch (event->key()) {
        case Qt::Key_Space : moving = !moving; break;
        case Qt::Key_R     : moving = false; initShapeMatchedObject(); break;
    case Qt::Key_Down: sys.z2 *= 0.9; break;
    case Qt::Key_Up: sys.z2 = 3.0; break;
		default: break;
	}

	updateGL();
}

int main (int argc, char **argv) {
	QApplication app(argc, argv);

	if (!QGLFormat::hasOpenGL()) {
		qWarning ("This system has no OpenGL support. Exiting.");
		return 1;
	}

	CGMainWindow *w = new CGMainWindow(NULL);

	w->show();

	return app.exec();
}

