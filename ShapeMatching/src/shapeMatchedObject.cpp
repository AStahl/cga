#include "shapeMatchedObject.h"

ShapeMatchedObject::ShapeMatchedObject(){

}

ShapeMatchedObject::ShapeMatchedObject(std::vector<Vector3d>& points, std::vector<int> &indices, std::vector<Vector3d>& v, std::vector<float>& w, float alpha, float beta){
    this->indices = std::vector<int>(indices);
    n = points.size();  //Anzahl der Punkte
    this->alpha = alpha;    //Steifheitsparameter (zwischen 0 und 1)
    this->beta =beta;
    x = points; // Orte der Partikel
    p.resize(n);
    this->v = v; // Geschwindigkeiten der Partikel
    this->w = w; // inverse Massen der Partikel (wenn 0, dann ist das Partikel unbeweglich)
    m.resize(n);         //Massen der Partikel für Shape Matching
    for(int i=0; i<n;i++) m[i] =1.0/w[i];
    k = 0.99;   //Dämpfungsfaktor (zwischen 0 und 1)

    gravity = -9.81;
    //gravity = 0.0;
    F_ext = std::vector<Vector3d>(n);   //externe Kräfte
    for (int i=0; i<n; i++) F_ext[i] = w[i] !=0? Vector3d(0,0,gravity/w[i]):Vector3d(0,0,0);

    //verschiebe alle Punkte der Ruhelage, sodass der Schwerpunkt im Ursprung liegt
    Vector3d massCenter = Vector3d(0.0,0.0,0.0);
    for(int i=0; i<n; i++)
        massCenter += points[i];
    massCenter /= n;
    x_0.resize(n);
    for(int i=0; i<n; i++)
        x_0[i] = points[i] - massCenter;    //undeformierte Ruhelage
}

void ShapeMatchedObject::makeStep(double dt){
    for(int i = 0; i < n; i++)
        v[i] += F_ext[i]*dt*w[i];    //update Geschwindigkeiten
    for(int i = 0; i < n; i++)
        v[i] *= k;   //simple dämpfung
    for(int i = 0; i < n; i++)
        p[i] = x[i]+v[i]*dt; //vorhersage der Partikelorte

    //berechne lineare Trafo (A,t)
    Matrix4d A;
    Vector3d t;
    shapeMatching(A,t);

    //transformiere x_0 mit (A,t) um die goal positions zu erhalten
    std::vector<Vector3d> g = std::vector<Vector3d>(n);
    for(int i=0; i<n; i++)
        g[i] = A*x_0[i] + t;

    //ziehe Partikel in Richtung der Goal Positions
    for(int i = 0; i < n; i++)
        p[i] = p[i] +  (g[i]-p[i])*alpha;

    //löse Kollisionen mit der z=0 Ebene auf
    for(int i = 0; i < n; i++)
        if(p[i].z() <0.0) p[i].z() = 0.0;

    //löse Kollisionen mit der z2 Ebene auf
    for(int i = 0; i < n; i++)
        if(p[i].z() >z2) p[i].z() = z2;

    for(int i = 0; i < n; i++) {
        v[i] = (p[i] - x[i])/dt;    //update Geschwindigkeiten
        x[i] = p[i];    //update Orte
    }
}

//berechnet das Matching von b=x-massCenter und a=x_0 und gibt
//den Translationsvektor t und die Matrix A zurück
void ShapeMatchedObject::shapeMatching(Matrix4d &R, Vector3d &t){
   ///add your code here

    t = Vector3d(0.0,0.0,0.0);
    for(int i=0; i<n; i++)
        t += p[i];
    t /= n;

    Matrix4d Apq; // = Matrix4d::identity();
    Matrix4d Aqq;
    Apq(0,0)=Apq(1,1)=Apq(2,2)=0;
    Aqq(0,0)=Aqq(1,1)=Aqq(2,2)=0;

    for(int i=0; i<n; i++){
        const Vector3d& pi = p[i]-t;
        const Vector3d& qi = x_0[i];
        for(int j=0;j<3;++j)
            for(int k=0; k<3;++k){
                Apq(j,k) += pi[j]*qi[k]/w[i];
                Aqq(j,k) += qi[j]*qi[k]/w[i];
            }
    }
    Aqq=Matrix4d::inverse(Aqq);

    Matrix4d A = Apq*Aqq;

    Matrix4d S2 = A.transpose()*A;

    Vector4d l;
    Matrix4d J;
    int n;
    S2.jacobi(l,J,n);

    Matrix4d S = J*Matrix4d(1.0/sqrt(l[0]),0,0,0,
                            0,1.0/sqrt(l[1]),0,0,
                            0,0,1.0/sqrt(l[2]),0,
                            0,0,0,1.0/sqrt(l[3]))*J.transpose();

    R=A*S;
}

void ShapeMatchedObject::draw(){

    //berechne smooth normals indem für jeden Vertex die
    //Normalen der angrenzenden Dreiecke gemittelt werden
    std::vector<Vector3d> normals = std::vector<Vector3d>(n);
    for(unsigned int i=0; i<indices.size();i+=3){
        Vector3d n = (x[indices[i+1]]-x[indices[i]])%(x[indices[i+2]]-x[indices[i]]);
        n.normalize();
        normals[indices[i]] += n;
        normals[indices[i+1]] += n;
        normals[indices[i+2]] += n;
    }
    for(int i=0; i<n;i++)
        normals[i].normalize();

    glBegin(GL_TRIANGLES);
    for(unsigned int i=0;i<indices.size();i+=3) {
        glNormal3dv(normals[indices[i]].ptr());
        glVertex3dv(x[indices[i]].ptr());
        glNormal3dv(normals[indices[i+1]].ptr());
        glVertex3dv(x[indices[i+1]].ptr());
        glNormal3dv(normals[indices[i+2]].ptr());
        glVertex3dv(x[indices[i+2]].ptr());
    }
    glEnd();
}
