#include "PBVFObj.h"
#include <cmath>

PBVFObj::PBVFObj(){}

PBVFObj::PBVFObj(std::vector<Vector3d> x0,
                 std::vector<Vector3d> v0,
                 double rho_0, double k, double k_near, double gravity,
                 double radius, Vector3d sphereCenter

                 )
{
    this->gravity = gravity;
    n = x0.size();
    P.resize(n);
    for(int i = 0; i < n; i++){
        P.at(i) = fluidParticle(x0.at(i), v0.at(i) );
    }

    //test
    //    for(int i = 0; i < n; i++){
    //        std::cout << i << ". P.at(i).x: " << P.at(i).x[0] << ", " << P.at(i).x[1] << ", " << P.at(i).x[2] << std::endl;
    //    }

    this->sphere.x = sphereCenter;
    this->sphere.m = 0.2;
    this->radius = radius;
    this->rho_0 = rho_0;
    this->k = k;
    this->k_near = k_near;

    h = 0.05;
    alpha = 0;
    beta = 1;
}

struct neighbours{
    Vector3d r_ij;
    int index;
    float q;
};


//Führt einen Diskretisionsschritt durch
void PBVFObj::makeStep(double dt){

    for(int i = 0; i < n; i++){
        P.at(i).v += Vector3d(0,-1,0)*gravity*dt;
    }
    sphere.v+=Vector3d(0,-1,0)*gravity*dt;

    // viscosity

    for(int i = 0; i < n; i++){
        for(int j = i+1; j < n; j++){
            Vector3d r_ij=P.at(i).x-P.at(j).x;
            float dist=r_ij.length();
            r_ij.normalize();
            float q=dist/h;
            if(q<1){
                float u=(P.at(i).v-P.at(j).v)*r_ij;
                if(u>0){
                    Vector3d dv=r_ij*dt*(1-q)*(u*alpha+beta*u*u)*0.5;
                    P.at(i).v-=dv;
                    P.at(j).v+=dv;
                }
            }
        }
    }

    // (explicit euler) prediction
    for(int i = 0; i < n; i++){
        P.at(i).x_prev = P.at(i).x;
        P.at(i).x += P.at(i).v*dt;
    }
    sphere.x_prev = sphere.x;
    sphere.x += sphere.v*dt;

    // double density relaxation

    for(int i = 0; i < n; i++){

        double rho=0;
        double rho_near=0;
        std::vector<neighbours> neighbourList;

        for(int j = 0; j < n; j++){
            if(j!=i){
                Vector3d r_ij=P.at(j).x-P.at(i).x;
                double dist=r_ij.length();
                r_ij.normalize();
                double q=dist/h;
                if(q<1){
                    rho+=(1-q)*(1-q);
                    rho_near+=(1-q)*(1-q)*(1-q);
                    neighbours neighbour;
                    neighbour.r_ij=r_ij; //P.at(j).x;
                    neighbour.index=j;
                    neighbour.q=q;
                    neighbourList.push_back(neighbour); //um die Nachbarn nicht nochmal finden zu müssen
                }
            }
        }

        double pressure=k*(rho-rho_0);
        double pressure_near=k_near*rho_near;
        Vector3d dx=Vector3d(0,0,0);

        for(unsigned int j = 0; j < neighbourList.size(); j++){
            double q=neighbourList[j].q;
            Vector3d displ= neighbourList[j].r_ij*dt*dt*(pressure*(1-q)+pressure_near*(1-q)*(1-q))*0.5;
            dx-=displ/2;
            P.at(neighbourList[j].index).x+=displ/2;
        }
        P.at(i).x+=dx;
    }


    //project sphere into plane
    Vector3d sphereCenter = sphere.x;
    sphereCenter[2]=0;
    double pRadius = (std::abs(sphere.x[2])>radius ? 0 : cos(std::abs(sphere.x[2])/radius)*radius);

    // generate Collisions with wall and sphere
    for(int i = 0; i < n; i++){
        if(P.at(i).x[0]>1){
            P.at(i).x[0]=1;
        }
        if(P.at(i).x[0]<0){
            P.at(i).x[0]=0;
        }
        if(P.at(i).x[1]<0){
            P.at(i).x[1]=0;
            if(std::rand()/RAND_MAX<0.01)
                P.at(i).x[1]+=0.001;
        }


        double d=(P.at(i).x-sphereCenter).length();

        if(d<pRadius)
        {
            Vector3d dif = (P.at(i).x-sphereCenter)*(pRadius/d-1);
            P.at(i).x+= dif*(P.at(i).m/(P.at(i).m+sphere.m));
            sphere.x-= dif*(sphere.m/(P.at(i).m+sphere.m));
        }

    }


    if((sphere.x[0]+radius)>1){
        sphere.x[0]=1-radius;
    }
    if((sphere.x[0]-radius)<0){
        sphere.x[0]=radius;
    }
    if((sphere.x[1]-radius)<0){
        sphere.x[1]=radius;
    }




    // update velocity in PBD fashion
    for(int i = 0; i < n; i++){
        P.at(i).v = (P.at(i).x-P.at(i).x_prev)/dt;
    }
    sphere.v = (sphere.x-sphere.x_prev)/dt;

}

//Zeichne die Partikel
void PBVFObj::draw(bool wireFrame){

    GLUquadricObj *quadric;
    quadric = gluNewQuadric();

    glPushMatrix();
    glScaled(2,2,2);

    glColor3d(1,0,0);
    glTranslated(sphere.x.x(), sphere.x.y(), sphere.x.z());
    gluSphere( quadric , radius-0.01 , 10 , 10);

    glPopMatrix();


    glColor3d(0,0,1);
    for(int i=0; i < n; i++){
        glPushMatrix();
        glScaled(2,2,2);
        glTranslated(P.at(i).x.x(),P.at(i).x.y(),P.at(i).x.z() );
        gluSphere( quadric , 0.01 , 10 , 10);
        glPopMatrix();
    }

}
