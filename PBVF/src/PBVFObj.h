#ifndef PBVFOBJ_H
#define PBVFOBJ_H

#include "vecmath.h"
#ifndef VECMATH_VERSION
#  error "vecmath-library might be out-of-date. Please use newer version"
#endif

#if _MSC_VER
    #include <gl/glu.h>
#elif __APPLE__
  #include <OpenGL/glu.h>
#else
    #include <GL/glu.h>
#endif

#include <QtOpenGL>
#include <iostream>
#include <vector>


class fluidParticle{
public:
    Vector3d x, x_prev, v;
    double m=1.0;
    fluidParticle(){}
    fluidParticle(const Vector3d x, const Vector3d v){
        this->x = x;
        this->v = v;

    }

};


class PBVFObj
{
public:
    PBVFObj();

    PBVFObj(std::vector<Vector3d> x0, std::vector<Vector3d> v0,
            double rho_0, double k, double k_near, double gravity,
            double radius, Vector3d sphereCenter
            );

    void makeStep(double dt);
    void draw(bool wireFrame);


    //Anzhal der Partikel
    int n;

    //position, vorhergesagte position und geschwindigkeit
    std::vector<fluidParticle> P;


    //Erdbeschleunigung
    double gravity;
    double rho_0;
    double k, k_near;
    double h;
    double alpha, beta;

    fluidParticle sphere;

    double radius;

    protected:
};
#endif //PBVFObj_H
