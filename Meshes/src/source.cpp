#include <QApplication>
#include <QMenu>
#include <QMenuBar>
#include <QStatusBar>
#include <QFileDialog>
#include <QGLWidget>
#include <QKeyEvent>
#include <QMessageBox>
#include <QTextEdit>
#include <QHBoxLayout>
#include "source.h"

#include <cmath>
#include <fstream>
#include <sstream>
#include <iostream>
#include <limits>

#ifdef max
#undef max
#endif

CGMainWindow* CGMainWindow::ptr;

CGMainWindow::CGMainWindow (QWidget* parent, Qt::WindowFlags flags)
				: QMainWindow (parent, flags) {
                                ptr = this;

                                showAABB=false;
                                collideAABB=false;
                                showOBB=false;
                                collideOBB=false;
                                showSplitOBB=false;
                                collideMesh=false;
                                showSphere=true;
                                showBVT=true;
                                collideBVT=true;

								resize (1024, 768);
								setWindowState(Qt::WindowMaximized);

								// Create a menu
								QMenu *file = new QMenu("&File",this);
								file->addAction ("Load Polyhedron", this, SLOT(loadPolyhedron()), Qt::CTRL+Qt::Key_G);
                                file->addAction ("Load Second Polyhedron", this, SLOT(loadSecondPolyhedron()), Qt::CTRL+Qt::Key_H);
								file->addAction ("Quit", qApp, SLOT(quit()), Qt::CTRL+Qt::Key_Q);
								menuBar()->addMenu(file);

                                // Create options menu
                                QMenu *view = new QMenu("&View",this);
                                QAction *showAABBAction = view->addAction ("Show AABB", this, SLOT(setShowAABB(bool)),Qt::Key_U);
                                QAction *collideAABBAction = view->addAction ("Collide AABB", this, SLOT(setCollideAABB(bool)),Qt::Key_J);
                                QAction *showOBBAction = view->addAction ("Show OBB", this, SLOT(setShowOBB(bool)),Qt::Key_I);
                                QAction *collideOBBAction = view->addAction ("Collide OBB", this, SLOT(setCollideOBB(bool)),Qt::Key_K);
                                QAction *showSplitOBBAction = view->addAction ("Show Split OBB", this, SLOT(setShowSplitOBB(bool)),Qt::Key_Comma);
                                QAction *collideMeshAction = view->addAction ("Collide Meshes", this, SLOT(setCollideMesh(bool)),Qt::Key_P);
                                QAction *showSphereAction = view->addAction ("Show Sphere", this, SLOT(setShowSphere(bool)),Qt::Key_Period);
                                QAction *showBVTAction = view->addAction ("Show BVT", this, SLOT(setShowBVT(bool)),Qt::Key_O);
                                QAction *collideBVTAction = view->addAction ("Collide BVT", this, SLOT(setCollideBVT(bool)),Qt::Key_L);
                                menuBar()->addMenu(view);

                                showAABBAction->setCheckable(true);
                                showAABBAction->setChecked(showAABB);
                                collideAABBAction->setCheckable(true);
                                collideAABBAction->setChecked(collideAABB);
                                showOBBAction->setCheckable(true);
                                showOBBAction->setChecked(showOBB);
                                collideOBBAction->setCheckable(true);
                                collideOBBAction->setChecked(collideOBB);
                                showSplitOBBAction->setCheckable(true);
                                showSplitOBBAction->setChecked(showSplitOBB);
                                collideMeshAction->setCheckable(true);
                                collideMeshAction->setChecked(collideMesh);
                                showSphereAction->setCheckable(true);
                                showSphereAction->setChecked(showSphere);
                                showBVTAction->setCheckable(true);
                                showBVTAction->setChecked(showBVT);
                                collideBVTAction->setCheckable(true);
                                collideBVTAction->setChecked(collideBVT);

								// Create a nice frame to put around the OpenGL widget
								QFrame* f = new QFrame (this);
								f->setFrameStyle(QFrame::Sunken | QFrame::Panel);
								f->setLineWidth(2);

								// Create our OpenGL widget
								ogl = new CGView (this,f);

								// Put the GL widget inside the frame
								QHBoxLayout* layout = new QHBoxLayout();
								layout->addWidget(ogl);
								layout->setMargin(0);
								f->setLayout(layout);

								setCentralWidget(f);

								statusBar()->showMessage("Ready",1000);
				}

CGMainWindow::~CGMainWindow () {}

void CGMainWindow::loadPolyhedron() {
    ogl->zoom = 1.0;

    ogl->obj1.loadPolyhedron();

    ogl->center = ogl->obj1.center()*ogl->obj1.zoom();

    if(ogl->obj1.getBVT()!=nullptr){
        /// Bauen wir die Testkugel!
        Vector3d a = ogl->obj1.getBVT()->ball().center*ogl->obj1.zoom();
        double d = ogl->obj1.getBVT()->ball().radius*ogl->obj1.zoom();
        double r = 0.2 * d;
        a += Vector3d (d,d,d)*0.5;
        Vector3d b = a + Vector3d (r,r,r);

        ogl->test_kugel = Sphere (a,b);
    }
    ogl->updateGL();


}

void CGMainWindow::loadSecondPolyhedron() {
                ogl->obj2.loadPolyhedron();

                ogl->updateGL();
}

void CGMainWindow::setShowAABB(bool b){showAABB=b;ogl->repaint(); }
void CGMainWindow::setCollideAABB(bool b){collideAABB=b;ogl->repaint();}
void CGMainWindow::setShowOBB(bool b){showOBB=b;ogl->repaint();}
void CGMainWindow::setCollideOBB(bool b){collideOBB=b;ogl->repaint();}
void CGMainWindow::setShowSplitOBB(bool b){showSplitOBB=b;ogl->repaint();}
void CGMainWindow::setCollideMesh(bool b){collideMesh=b;ogl->repaint();}
void CGMainWindow::setShowSphere(bool b){showSphere=b;ogl->repaint();}
void CGMainWindow::setShowBVT(bool b){showBVT=b;ogl->repaint();}
void CGMainWindow::setCollideBVT(bool b){collideBVT=b;ogl->repaint();}

void CGMainWindow::keyPressEvent(QKeyEvent* event) {

    static const Quat4d xRot = Quat4d(0.1,Vector3d(1,0,0));
    static const Quat4d yRot = Quat4d(0.1,Vector3d(0,1,0));
    static const Quat4d zRot = Quat4d(0.1,Vector3d(0,0,1));
    static const float tr = 0.005;
    static const Vector3d x = Vector3d(tr,0,0);
    static const Vector3d y = Vector3d(0,tr,0);
    static const Vector3d z = Vector3d(0,0,tr);

    switch (event->key()) {
    case Qt::Key_A:     ogl->obj2.move(-x); break;
    case Qt::Key_D:     ogl->obj2.move(x); break;
    case Qt::Key_S:     ogl->obj2.move(-y); break;
    case Qt::Key_W:     ogl->obj2.move(y); break;
    case Qt::Key_Q:     ogl->obj2.move(-z); break;
    case Qt::Key_E:     ogl->obj2.move(z); break;
    case Qt::Key_F:     ogl->obj2.rotate(yRot.conjugate()); break;
    case Qt::Key_H:     ogl->obj2.rotate(yRot); break;
    case Qt::Key_T:     ogl->obj2.rotate(xRot.conjugate()); break;
    case Qt::Key_G:     ogl->obj2.rotate(xRot); break;
    case Qt::Key_R:     ogl->obj2.rotate(zRot.conjugate()); break;
    case Qt::Key_Z:     ogl->obj2.rotate(zRot); break;
    //case Qt::Key_Space : picked = (picked+1)%numpoints; changed=false;break;
    case Qt::Key_X:     if (event->modifiers() & Qt::ShiftModifier) ogl->test_kugel.center[0] -= 0.05; else ogl->test_kugel.center[0] += 0.05; break;
    case Qt::Key_Y:     if (event->modifiers() & Qt::ShiftModifier) ogl->test_kugel.center[1] -= 0.05; else ogl->test_kugel.center[1] += 0.05; break;
    case Qt::Key_C:     if (event->modifiers() & Qt::ShiftModifier) ogl->test_kugel.center[2] -= 0.05; else ogl->test_kugel.center[2] += 0.05; break;
    case Qt::Key_N:     if (ogl->obj1.drawDeep>0){ --ogl->obj1.drawDeep;}if (ogl->obj2.drawDeep>0){ --ogl->obj2.drawDeep;} break;
    case Qt::Key_M:     ++ogl->obj1.drawDeep; ++ogl->obj2.drawDeep; break;
    default: break; //changed=false; break;
    }
    ogl->updateGL();
}

CGView::CGView (CGMainWindow *mainwindow,QWidget* parent ) : QGLWidget (parent) {
                main = mainwindow;
}

void CGView::drawMesh(){
    Intersect interAABB=UNKNOWN,interOBB=UNKNOWN;

    obj1.clearIntersect();
    obj2.clearIntersect();

    if(CGMainWindow::ptr->collideAABB){
        try{
            if(obj1.getAABB().intersect(obj2.getAABB()))
                interAABB=INTERSECT;
            else
                interAABB=NO_INTERSECT;
        }
        catch(...){}
    }

    if(CGMainWindow::ptr->collideOBB){
        try{
            if(obj1.getOBB().intersect(obj2.getOBB()))
                interOBB=INTERSECT;
            else
                interOBB=NO_INTERSECT;
        }
        catch(...){}
    }

    if(CGMainWindow::ptr->collideMesh){
        try{
            obj1.intersect(obj2);
        }
        catch(...){}
    }

    if(CGMainWindow::ptr->showSphere){
        try{
            test_kugel.draw();

            if(obj1.getBVT()!=nullptr)
                obj1.getBVT()->intersect(test_kugel);

            if(obj2.getBVT()!=nullptr)

                obj2.getBVT()->intersect(test_kugel);

        }
        catch(...){}
    } else {
        try{
            if(obj1.getBVT()!=nullptr)
                obj1.getBVT()->noIntersect();

            if(obj2.getBVT()!=nullptr)
                obj2.getBVT()->noIntersect();

        }
        catch(...){}
    }

    if(obj1.getBVT()!=nullptr&&obj2.getBVT()!=nullptr&&CGMainWindow::ptr->collideBVT){
        try{
            obj1.getBVT()->intersect(*(obj2.getBVT()));
        }
        catch(...){}

    }

    obj1.draw(0,0,1,interAABB,interOBB,0,0,0);
    obj2.draw(1,0,0,interAABB,interOBB,1,0,1);
}

void CGView::initializeGL() {
				qglClearColor(Qt::white);
				zoom = 1.0;
				center = 0.0;
				q_now = Quat4d(0.0,0.0,0.0,1.0);
				glEnable(GL_DEPTH_TEST);
				glEnable(GL_COLOR_MATERIAL);
				glEnable(GL_LIGHT0);
				glEnable(GL_LIGHTING);
				glEnable(GL_BLEND);
				glEnable(GL_NORMALIZE);
				glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
				float pos[4] = {0,2,2,1};
				glLightfv(GL_LIGHT0,GL_POSITION, pos);

}

void CGView::paintGL() {
				glMatrixMode(GL_MODELVIEW);
				glLoadIdentity();
                glTranslatef(0,0,-2);


				Matrix4d R(q_now);
				Matrix4d RT = R.transpose();
                glMultMatrixd(RT.ptr());

				glScaled(zoom,zoom,zoom);
				glTranslated(-center[0],-center[1],-center[2]);

				glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

				drawMesh();

}

void CGView::resizeGL(int width, int height) {
				glViewport(0,0,width,height);
				glMatrixMode(GL_PROJECTION);
				glLoadIdentity();
				if (width > height) {
								double ratio = width/(double) height;
								//glOrtho(-ratio,ratio,-1.0,1.0,-10.0,10.0);
								//glFrustum(-ratio,ratio,-1.0,1.0,1.0,10.0);
								gluPerspective(60,ratio,0.1,100.0);
								//gluLookAt(0,0,-100,0,0,0,0,1,0);
				}
				else {
								double ratio = height/(double) width;
								//glOrtho(-1.0,1.0,-ratio,ratio,-10.0,10.0);
								//glFrustum(-1.0,1.0,-ratio,ratio,1.0,10.0);
								gluPerspective(60,1/ratio,0.1,100.0);
								//		glFrustum(-1.0,1.0,-ratio,ratio,1.0,100.0);
								//		gluPerspective(fov,1.0/ratio,1.0,10000.0);
								//gluLookAt(0,0,-1,0,0,1,0,1,0);
				}
				glMatrixMode (GL_MODELVIEW);
}

void CGView::mousePressEvent(QMouseEvent *event) {
				oldX = event->x();
				oldY = event->y();
				if( event->button() == Qt::LeftButton )
				{
								buttonState = 0;

				}
				else if( event->button() == Qt::RightButton )
				{
								buttonState = 1;
				}
				else
								buttonState = 2;	

}

void CGView::mouseReleaseEvent(QMouseEvent*) {}

void CGView::wheelEvent(QWheelEvent* event) {
				if (event->delta() < 0) zoom *= 1.2; else zoom *= 1/1.2;
				update();
}

void CGView::mouseMoveEvent(QMouseEvent* event) {

				//if( event->button() == Qt::LeftButton )
				if( buttonState == 0 )	/// rotate camera
				{
								Vector3d p1,p2;

								mouseToTrackball(oldX,oldY,width(),height(),p1);
								mouseToTrackball(event->x(),event->y(),width(),height(),p2);

								Quat4d q = trackball(p1,p2);
								q_now = q * q_now;
								q_now.normalize();
				}
				else if( buttonState == 1 ) /// translate object
				{
								float dx,dy;
								dx = dy = .001f;
								Matrix4d R(q_now);
								Matrix4d RT = R.transpose();

								Vector3d v(0,0,0);
								v[0] = -dx*(event->x() - oldX);
								v[1] =  dy*(event->y() - oldY);
								v = RT*v;

								center += v;
								//cRef -= v;
				}


				oldX = event->x();
				oldY = event->y();

				updateGL();
}

void CGView::mouseToTrackball(int x, int y, int W, int H, Vector3d &v) {
				if (W > H) {
								v[0] = (2.0*x-W)/H;
								v[1] = 1.0-y*2.0/H;
				} else {
								v[0] = (2.0*x-W)/W;
								v[1] = (H-2.0*y)/W;
				}
				double d = v[0]*v[0]+v[1]*v[1];
				if (d > 1.0) {
								v[2] = 0.0;
								v /= sqrt(d);
				} else v[2] = sqrt(1.0-d*d);
}

Quat4d CGView::trackball(const Vector3d& u, const Vector3d& v) {
				Vector3d uxv = u % v;
				Quat4d ret(uxv[0],uxv[1],uxv[2],1.0+u*v);
				ret.normalize();
				return ret;
}


int main (int argc, char **argv) {
				QApplication app(argc, argv);

				if (!QGLFormat::hasOpenGL()) {
								qWarning ("This system has no OpenGL support. Exiting.");
								return 1;
				}

				CGMainWindow *w = new CGMainWindow(NULL);

				w->show();

				return app.exec();
}

