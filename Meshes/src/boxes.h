#pragma once

#include <vector>
#include <set>
#include "vecmath.h"



#if _MSC_VER
    #include <gl/glu.h>
#elif __APPLE__
  #include <OpenGL/glu.h>
#else
    #include <GL/glu.h>
#endif

//#ifndef BOXES_H
//#define BOXES_H

enum Intersect {INTERSECT,NO_INTERSECT,UNKNOWN};

class AABB{
public:
    AABB(const std::vector<Vector3d>& p);
    AABB(Vector3d min, Vector3d max);

    Vector3d min,max;

    void draw(Intersect intersect = UNKNOWN);
    bool intersect (const AABB& B);

    AABB operator+(const Vector3d &b) const;
    AABB operator*(const double &b) const;
};

class OBB{
public:
    OBB();
    OBB(const std::vector<Vector3d>& p);
    OBB(const std::vector<Vector3d>& p,const Vector3d& center);
    OBB(const std::vector<Vector3d>* P,const std::set<size_t> indices, const Vector3d center,const Vector3d a[3],const double alpha[3]);
    void calc(const std::vector<Vector3d>& p,const std::set<size_t> indices);
    void calc(const std::vector<Vector3d>& p,const Vector3d& center);

    const std::vector<Vector3d>* P;
    std::set<size_t> indices;

    Vector3d center;
    Vector3d a[3];
    double alpha[3];

    void vertex(int x, int y, int z){
        return glVertex3dv((a[0]*alpha[0]*x+a[1]*alpha[1]*y+a[2]*alpha[2]*z).ptr());
    }
    void draw(Intersect intersect = UNKNOWN);
    bool intersect (const OBB& B);

    OBB operator+(const Vector3d& b)const;
    OBB operator*(const double& b)const;
    OBB operator*(const Matrix4d& b)const;

    static void splitOBB(const OBB& A, OBB& A1, OBB& A2);
    void splitOBB(OBB& A1, OBB& A2) const;
};
//#endif // BOXES_H
