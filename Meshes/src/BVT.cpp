#include "BVT.h"

#include "Sphere.h"
#include "mesh.h"

#include <iostream>
#include <algorithm>
#include <map>

#define maximal_points 10
using namespace std;

// Construktor
BVT::BVT(Mesh* const mesh)
    :mesh(mesh),left_(nullptr),right_(nullptr)
{
    for(size_t i=0; i<mesh->tr.size();++i)
        trInd_.insert((int)i);

    create();
}

BVT::BVT(Mesh* const mesh,
         const std::set<int>& trIndices)
    :mesh(mesh),trInd_(trIndices),left_(nullptr),right_(nullptr)
{
    create();
}

void BVT::create()
{
    {
        std::vector<Vector3d> points;
        for(auto it=trInd_.begin(); it!=trInd_.end();++it){
            points.push_back(mesh->point(*it,0));
            points.push_back(mesh->point(*it,1));
            points.push_back(mesh->point(*it,2));
        }
        ball_.calc(points);
    }

    if(trInd_.size()==1){
        mass_center_ = mesh->point(*trInd_.begin(),0);
        mass_center_ += mesh->point(*trInd_.begin(),1);
        mass_center_ += mesh->point(*trInd_.begin(),2);
        mass_center_ /=3;
        return;
    }

    //for our split we need a weighted mass center
  for(auto it=trInd_.begin(); it!=trInd_.end();++it){
    mass_center_ += mesh->point(*it,0);
    mass_center_ += mesh->point(*it,1);
    mass_center_ += mesh->point(*it,2);
  }
  mass_center_ /= trInd_.size()*3;

	// Compute inetria matrix
  for(auto it=trInd_.begin(); it!=trInd_.end();++it) {
      for(int j=0;j<3;++j){
          const Vector3d& r = mesh->point(*it,j) - mass_center_;
          inertia_(0,0) += r[1]*r[1]+r[2]*r[2];
          inertia_(1,1) += r[0]*r[0]+r[2]*r[2];
          inertia_(2,2) += r[0]*r[0]+r[1]*r[1];
          inertia_(1,0)=inertia_(0,1) -= r[0]*r[1];
          inertia_(2,0)=inertia_(0,2) -= r[0]*r[2];
          inertia_(2,1)=inertia_(1,2) -= r[1]*r[2];
      }
  }
 
	/// Due to the fact that we work with Matrix4d, expand the last row/column by (0,0,0,1)
  inertia_(0,3) = inertia_(3,0) = 0.0;
  inertia_(1,3) = inertia_(3,1) = 0.0;
  inertia_(2,3) = inertia_(3,2) = 0.0;
  inertia_(3,3) = 1.0;

    split();
}

/********************************************************
** Construcs an optimal splitting of points_ into two disjoint sets
** links and rechts and increases the BVT-tree by 1. 
** The splitting follows the idea of the lecture:
** (1) Compute the inertia matrix M of points_
** (2) Compute the eigenvalues/eigenvectors of M by M.jacoby() (vecmath!)
** (3) Let v be the most stable eigenvector and c be the mass center.
**     Split points_ by plane p: (x-c)^T*v = 0 
*********************************************************/
void BVT::split ()
{
    if(trInd_.size()<=10) return;//10 triangles

    // Computes the eigenvalues/vectors from the inertia matrix
	Vector4d eigenvalue;
	Matrix4d eigenvector;
	int nrot; /// not important

	/// Dont forget to compute inertia_ first!
	inertia_.jacobi (eigenvalue, eigenvector, nrot);

    //Find out which eigenvalue is the largest
    int n = 0;
    for(int i = 0; i<3; i++)
        if(eigenvalue[i]<eigenvalue[n])
            n=i;
    //Find the corresponding eigenvector
    Vector3d splitAxis{
        eigenvector(n,0),
        eigenvector(n,1),
        eigenvector(n,2)
    };

    //determine which triangles belong to which side
    std::set<int> links;
    std::set<int> rechts;
   for(auto it=trInd_.begin(); it!=trInd_.end();++it){

        double side=0;
        side+=(mesh->point(*it,0)-mass_center_)*splitAxis;
        side+=(mesh->point(*it,1)-mass_center_)*splitAxis;
        side+=(mesh->point(*it,2)-mass_center_)*splitAxis;

        if(side<0){//push to left
            links.insert(*it);
        }else{//push to right
            rechts.insert(*it);
        }
    }

    if(links.size()==0 || rechts.size()==0){
        std::cout << "Unsplittable: "<<trInd_.size()<< " triangles left";
        return;
    }

	/// Kinder sind wieder Baeume! Wichtig das NEW!
    //if(links.size()>1)
        left_ = new BVT (mesh,links);
    //if(rechts.size()>1)
        right_ = new BVT (mesh,rechts);
}

void BVT::noIntersect(){//reset intersection bool, no calculations needed
    ball().intersects=false;
    if(left_!=nullptr)
        left_->noIntersect();
    if(right_!=nullptr)
        right_->noIntersect();
}

bool BVT::intersect(const Sphere &S){
    Vector3d centerB = ball().center*mesh->zoom()*mesh->rotation()+mesh->pos();

    double radiusB = ball().radius*mesh->zoom();


    if(
            (S.center-centerB).length()<(radiusB+S.radius)
            ) //intersection
    {
        ball().intersects=true;
        if(left_!=nullptr)
            left_->intersect(S);
        if(right_!=nullptr)
            right_->intersect(S);
        return true;
    }else{
        noIntersect();
        return false;
    }
}

bool BVT::intersect(BVT &B){
    Vector3d centerA = ball().center*mesh->zoom()*mesh->rotation()+mesh->pos();
    Vector3d centerB = B.ball().center*B.mesh->zoom()*B.mesh->rotation()+B.mesh->pos();

    double radiusA = B.ball().radius*B.mesh->zoom();
    double radiusB = B.ball().radius*B.mesh->zoom();

    if((centerA-centerB).length()<(radiusA+radiusB)){//Spheres intersecting
        if(left()!=nullptr && right()!=nullptr&&B.left()!=nullptr && B.right()!=nullptr){
            //both are nodes
            if(radiusA>radiusB){ //split the bigger one first
                left()->intersect(B);
                right()->intersect(B);
            }else{
                B.left()->intersect(*this);
                B.right()->intersect(*this);
            }
        }else if(left()!=nullptr && right()!=nullptr){
            //this one is a node, but not B
            left()->intersect(B);
            right()->intersect(B);
        }else if(B.left()!=nullptr && B.right()!=nullptr){
            //B is a node, but this one isnt
            B.left()->intersect(*this);
            B.right()->intersect(*this);
        }else{
            //both are leaves
            std::vector<Vector3d> pa, pb;
            for(auto itA=trInd_.begin();itA!=trInd_.end();++itA) {
                for(auto itB=B.trInd_.begin();itB!=B.trInd_.end();++itB) {
                    pa={
                        (mesh->point(*itA,0))*mesh->zoom()*Matrix4d(mesh->rotation()).transpose()+mesh->pos(),
                        (mesh->point(*itA,1))*mesh->zoom()*Matrix4d(mesh->rotation()).transpose()+mesh->pos(),
                        (mesh->point(*itA,2))*mesh->zoom()*Matrix4d(mesh->rotation()).transpose()+mesh->pos(),
                    };
                    pb={
                        (B.mesh->point(*itB,0))*B.mesh->zoom()*Matrix4d(B.mesh->rotation()).transpose()+B.mesh->pos(),
                        (B.mesh->point(*itB,1))*B.mesh->zoom()*Matrix4d(B.mesh->rotation()).transpose()+B.mesh->pos(),
                        (B.mesh->point(*itB,2))*B.mesh->zoom()*Matrix4d(B.mesh->rotation()).transpose()+B.mesh->pos(),
                    };
                    if(triangleIntersect(pa,pb)){
                        mesh->tr[*itA].intersects=true;
                        B.mesh->tr[*itB].intersects=true;
                    }
                }
            }
        }
        return true;

    }else{
        return false;
    }

}
