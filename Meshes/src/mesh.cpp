#include "mesh.h"
#include "OffReader.h"
#include "source.h"
#include <QString>
#include <QFileDialog>
#include <QStatusBar>



void Mesh::loadPolyhedron() {
    QString filename = QFileDialog::getOpenFileName(CGMainWindow::ptr, "Load generator model ...", QString(), "OFF files (*.off)" );

    if (filename.isEmpty()) return;
    CGMainWindow::ptr->statusBar()->showMessage ("Loading model ...");

    P.clear();
    tr.clear();

    LoadOffFile(filename.toStdString().c_str(),P,tr);


    center_ = Vector3d(0,0,0);
    Vector3d min = +std::numeric_limits<double>::max();
    Vector3d max = -std::numeric_limits<double>::max();
    for(size_t i=0;i<P.size();i++) {
        center_ += P[i];
        for (int j=0;j<3;++j){
            min[j]=std::min(min[j],P[i][j]);
            max[j]=std::max(max[j],P[i][j]);
        }

    }
    center_/=P.size();
    zoom_ = 1.0/std::max(std::max(max[0]-min[0],max[1]-min[1]),max[2]-min[2]);

    recalcAABB();

    obb.reset(new OBB(P,center()));

    obbSplit[0].reset(new OBB());
    obbSplit[1].reset(new OBB());
    obb->splitOBB(*obbSplit[0],*obbSplit[1]);

    bvt.reset(new BVT (this));

    CGMainWindow::ptr->statusBar()->showMessage ("Loading generator model done." ,3000);
}

void Mesh::move(const Vector3d mv){
    pos_ += mv;
}
void Mesh::rotate(const Quat4d q){
    rot_*=q;
    recalcAABB();
}
void Mesh::scale(const double s){
    zoom_*=s;
}

void Mesh::draw(int r, int g, int b, Intersect interAABB, Intersect interOBB, int iR, int iG, int iB){
    glPushMatrix(); //translate + scale mesh + boxes
    glTranslated(pos_[0],pos_[1],pos_[2]);
    glScaled(zoom_,zoom_,zoom_);

    glPushMatrix(); //rotate mesh
    Matrix4d R(rot_);
    Matrix4d RT = R.transpose();
    glMultMatrixd(RT.ptr());

    glColor3d(r,g,b);
    Vector3d n;

    //skip constant color changes if not colliding
    if(CGMainWindow::ptr->collideMesh||CGMainWindow::ptr->collideBVT){
        glBegin(GL_TRIANGLES);
        for(auto it=tr.begin();it!=tr.end();++it) {
            if(it->intersects){
                glColor3d(iR,iG,iB);
            }else{
                glColor3d(r,g,b);
            }
            n.cross((P[(*it)[1]]-P[(*it)[0]]),(P[(*it)[2]]-P[(*it)[0]]));
            n.normalize(n);
            glNormal3dv(n.ptr());
            glVertex3dv(P[(*it)[0]].ptr());
            glVertex3dv(P[(*it)[1]].ptr());
            glVertex3dv(P[(*it)[2]].ptr());
        }
        glEnd();
    }else{
        glBegin(GL_TRIANGLES);
        for(auto it=tr.begin();it!=tr.end();++it) {
            n.cross((P[(*it)[1]]-P[(*it)[0]]),(P[(*it)[2]]-P[(*it)[0]]));
            n.normalize(n);
            glNormal3dv(n.ptr());
            glVertex3dv(P[(*it)[0]].ptr());
            glVertex3dv(P[(*it)[1]].ptr());
            glVertex3dv(P[(*it)[2]].ptr());
        }
        glEnd();
    }

    if(obb!=nullptr && CGMainWindow::ptr->showOBB)
        obb->draw(interOBB);

    if(obbSplit[0]!=nullptr && CGMainWindow::ptr->showSplitOBB)
        obbSplit[0]->draw(UNKNOWN);

    if(obbSplit[1]!=nullptr && CGMainWindow::ptr->showSplitOBB)
        obbSplit[1]->draw(UNKNOWN);

    if(CGMainWindow::ptr->showBVT){
        drawBall();
    }

    glPopMatrix(); //rotation

    if(aabb!=nullptr && CGMainWindow::ptr->showAABB)
        aabb->draw(interAABB);
    glPopMatrix(); //translation+scale
}

/// Rekursiver abstieg bis in Tiefe deep zum malen des Huellkoerpers
void Mesh::drawBall (BVT * tree, int deep)
{
    if(tree==nullptr)return;

    if (deep<0){
        deep=drawDeep;
    }

    if (deep==0){
        if(tree->ball().intersects){
            tree->ball().draw(Vector3d(1,0,0));
        } else{
            tree->ball().draw();
        }
    } else{
        if(tree->left()==nullptr&&tree->right()==nullptr){ //we have a leaf and try to draw deeper objects
            //tree->ball().draw(Vector3d(0.1,0.1,0.1));
        }
        if(tree->left()!=nullptr){
            drawBall(tree->left(),deep-1);
        }
        if(tree->right()!=nullptr){
            drawBall(tree->right(),deep-1);
        }
    }
}

AABB Mesh::getAABB(){
    if(aabb==nullptr)
        throw "Not set yet";

    return (*aabb)*zoom_+pos_;
}

OBB Mesh::getOBB(){
    if(obb==nullptr)
        throw "Not set yet";

    return (((*obb)*zoom_)*(Matrix4d(rot_))+pos_);
}

void Mesh::recalcAABB(){

    std::vector<Vector3d> rotP;

    for(size_t i=0;i<P.size();i++){
        rotP.push_back(rot_*P[i]);
    }

    aabb.reset(new AABB(rotP));
}

bool triangleIntersect(std::vector<Vector3d> pa,std::vector<Vector3d> pb){

    //find all possible vectors
    std::vector<Vector3d> axes;
    //normals
    Vector3d na=(pa[2]-pa[1])%(pa[1]-pa[0]);
    Vector3d nb=(pb[2]-pb[1])%(pb[1]-pb[0]);
    axes.push_back(na);
    axes.push_back(nb);

    for(int i=0;i<3;++i){
        //border normals
        axes.push_back((pa[(i+1)%3]-pa[i])%na);
        axes.push_back((pb[(i+1)%3]-pb[i])%nb);
        for(int j=0;j<3;++j){
            //cross products of borders
            axes.push_back((pa[(i+1)%3]-pa[i])%(pb[(j+1)%3]-pb[j]));
        }
    }

    //remove 0-vectors
    //for(auto it=axes.begin();it!=axes.end();++it){
    //    if (epsilonEquals(*it,Vector3d(0,0,0))){
    //        it=axes.erase(it);
    //    }
    //}

    //possible improvement: find parallel vectors and erase them

    //check vectors
    double minA,maxA,minB,maxB;
    for(auto sep=axes.begin();sep!=axes.end();++sep){
        minA = std::numeric_limits<double>::max();
        maxA = -std::numeric_limits<double>::max();
        minB = std::numeric_limits<double>::max();
        maxB = -std::numeric_limits<double>::max();

        //Triangle Points
        for(int i=0;i<3;++i){
            minA=std::min(minA,pa[i]*(*sep));
            maxA=std::max(maxA,pa[i]*(*sep));
            minB=std::min(minB,pb[i]*(*sep));
            maxB=std::max(maxB,pb[i]*(*sep));
        }

        //standard interval intersection test -> if none, we found a separating axes
        if(minA>maxB||minB>maxA)
            return false;

    }

    //none found -> intersecting
    return true;

}

void Mesh::clearIntersect(){
    for(auto it=tr.begin();it!=tr.end();++it)
        it->intersects=false;
}

void Mesh::intersect(Mesh& B){
    std::vector<Vector3d> pa, pb;
    for(size_t a=0;a<tr.size();a++) {
        for(size_t b=0;b<B.tr.size();b++){
            pa={
                (point(a,0))*zoom_*Matrix4d(rot_).transpose()+pos_,
                (point(a,1))*zoom_*Matrix4d(rot_).transpose()+pos_,
                (point(a,2))*zoom_*Matrix4d(rot_).transpose()+pos_
            };
            pb={
                (B.point(b,0))*B.zoom_*Matrix4d(B.rot_).transpose()+B.pos_,
                (B.point(b,1))*B.zoom_*Matrix4d(B.rot_).transpose()+B.pos_,
                (B.point(b,2))*B.zoom_*Matrix4d(B.rot_).transpose()+B.pos_
            };
            if(triangleIntersect(pa,pb)){
                tr[a].intersects=true;
                B.tr[b].intersects=true;
            }
        }
    }
}
