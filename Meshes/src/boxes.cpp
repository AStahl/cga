#include "boxes.h"

#include <limits>
#include <iostream>

void setIntersectColor(Intersect intersect){
    if(intersect==NO_INTERSECT)
        glColor3d(0.5,1,0.5);
    else if(intersect==INTERSECT)
        glColor3d(1,0,0);
    else
        glColor3d(0,0,0);
}



AABB::AABB(const std::vector<Vector3d> &p){
    min = +std::numeric_limits<double>::max();
    max = -std::numeric_limits<double>::max();

    for(size_t i=0;i<p.size();++i){
        for (int j=0;j<3;++j){
            min[j]=std::min(min[j],p[i][j]);
            max[j]=std::max(max[j],p[i][j]);
        }
    }
}

AABB::AABB(Vector3d mi,Vector3d ma){
    min = mi;
    max = ma;
}

bool AABB::intersect(const AABB &B){
    for(int j=0;j<3;++j){
        if(min[j]>B.max[j]||B.min[j]>max[j])
            return false;
    }
    return true;
}

AABB AABB::operator+(const Vector3d& b)const{
    return AABB(min+b,max+b);
}

AABB AABB::operator*(const double& b)const{
    return AABB(min*b,max*b);
}

void AABB::draw(Intersect intersect){
    setIntersectColor(intersect);

    glLineWidth(5.0);
    glPushAttrib(GL_LIGHTING);
    glDisable(GL_LIGHTING);

    glBegin(GL_LINE_LOOP);
    glVertex3d(min[0],min[1],min[2]);
    glVertex3d(max[0],min[1],min[2]);
    glVertex3d(max[0],max[1],min[2]);
    glVertex3d(min[0],max[1],min[2]);
    glEnd();

    glBegin(GL_LINE_LOOP);
    glVertex3d(min[0],min[1],max[2]);
    glVertex3d(max[0],min[1],max[2]);
    glVertex3d(max[0],max[1],max[2]);
    glVertex3d(min[0],max[1],max[2]);
    glEnd();


    glBegin(GL_LINES);
    glVertex3d(min[0],min[1],min[2]);
    glVertex3d(min[0],min[1],max[2]);

    glVertex3d(max[0],min[1],min[2]);
    glVertex3d(max[0],min[1],max[2]);

    glVertex3d(max[0],max[1],min[2]);
    glVertex3d(max[0],max[1],max[2]);

    glVertex3d(min[0],max[1],min[2]);
    glVertex3d(min[0],max[1],max[2]);
    glEnd();

    glPopAttrib();

}


OBB::OBB(){}

//ermöglicht den Konstruktor mit einem Argument aufzurufen wie gefordert
//ist allerdings doppelte berechnung, da wir center ja schon kennen...
OBB::OBB(const std::vector<Vector3d> &p){
    //no indices given -> consider all of them
    //no center given -> calculate
    Vector3d center;

    for(size_t i=0;i<p.size();++i){
        center+=p[i];
        indices.insert(i);
    }

    center/=p.size();

    calc(p,center);
}

//erspart uns die doppelte Berechnung von center
OBB::OBB(const std::vector<Vector3d> &p, const Vector3d &center){
    //no indices given -> consider all of them
    for(size_t i=0;i<p.size();++i){
        indices.insert(i);
    }

    calc(p,center);
}

//"copy"-constructor
OBB::OBB(const std::vector<Vector3d>* P,const std::set<size_t> indices, const Vector3d center,const Vector3d a[3],const double alpha[3])
    :P(P),indices(indices),center(center)
{
    this->a[0]=a[0];
    this->a[1]=a[1];
    this->a[2]=a[2];
    this->alpha[0]=alpha[0];
    this->alpha[1]=alpha[1];
    this->alpha[2]=alpha[2];
}


OBB OBB::operator+(const Vector3d& b)const{
    return OBB(P,indices,center+b,a,alpha);
}

OBB OBB::operator*(const double& b)const{
    double newAlpha[3];
    newAlpha[0]=alpha[0]*b;
    newAlpha[1]=alpha[1]*b;
    newAlpha[2]=alpha[2]*b;
    return OBB(P,indices,center*b,a,newAlpha);
}

OBB OBB::operator*(const Matrix4d& b)const{
    Vector3d newA[3];
    newA[0]=b*a[0];
    newA[1]=b*a[1];
    newA[2]=b*a[2];
    return OBB(P,indices,b*center,newA,alpha);
}

void OBB::calc(const std::vector<Vector3d> &p, const std::set<size_t> indices){
    this->indices=indices;
    //no center given -> calculate
    Vector3d center;

    for(size_t i=0;i<p.size();++i){
        center+=p[i];
    }

    center/=p.size();

    calc(p,center);
}

void OBB::calc(const std::vector<Vector3d> &p, const Vector3d &center){
    P = &p;

    Matrix4d C;
    C(0,0)=0;
    C(1,1)=0;
    C(2,2)=0;
    C(3,3)=0;

    for(auto n=indices.begin();n!=indices.end();++n){
        for(size_t i=0;i<3;++i){
            for(size_t j=0;j<3;++j){
                C(i,j) += ((p[*n]-center)[i])*((p[*n]-center)[j]);
            }
        }
    }

    Matrix4d V;
    Vector4d d;
    int nrot;

    C.jacobi(d,V,nrot);


    for(size_t i=0;i<3;++i){
        for(size_t j=0;j<3;++j){
            a[i][j]=V(j,i);
        }
        a[i].normalize();
    }

    Vector3d alphamin = +std::numeric_limits<double>::max();
    Vector3d alphamax = -std::numeric_limits<double>::max();

    for(auto n=indices.begin();n!=indices.end();++n){
        for(size_t i=0;i<3;++i){
            alphamin[i]=std::min(alphamin[i],a[i]*(p[*n]));
            alphamax[i]=std::max(alphamax[i],a[i]*(p[*n]));
        }
    }

    this->center=Vector3d(0,0,0);
    for(size_t i=0;i<3;++i){
        this->center+=a[i]*(alphamax[i]+alphamin[i])/2;
        alpha[i]=(alphamax[i]-alphamin[i])/2;
        //std::cout << alpha[i] << "\n";
    }

}

bool OBB::intersect (const OBB& B){
    //find all possible vectors
    std::vector<Vector3d> axes;
    for(int i=0;i<3;++i){
        axes.push_back(a[i]);
        axes.push_back(B.a[i]);
        for(int j=0;j<3;++j){
            axes.push_back(a[i]%B.a[j]);
        }
    }

    //remove 0-vectors
    for(std::vector<Vector3d>::iterator it=axes.begin();it!=axes.end();++it){
        if (epsilonEquals(*it,Vector3d(0,0,0))){
            it=axes.erase(it);
        }
    }

    //possible improvement: find parallel vectors and erase them

    //check vectors
    double minA,maxA,minB,maxB;
    for(std::vector<Vector3d>::iterator sep=axes.begin();sep!=axes.end();++sep){
        minA = std::numeric_limits<double>::max();
        maxA = -std::numeric_limits<double>::max();
        minB = std::numeric_limits<double>::max();
        maxB = -std::numeric_limits<double>::max();

        //all combinations of +-1
        for(int i=-1;i<2;i+=2)
            for(int j=-1;j<2;j+=2)
                for(int k=-1;k<2;k+=2){
                    minA=std::min(minA,(center+a[0]*alpha[0]*i+a[1]*alpha[1]*j+a[2]*alpha[2]*k)*(*sep));
                    maxA=std::max(maxA,(center+a[0]*alpha[0]*i+a[1]*alpha[1]*j+a[2]*alpha[2]*k)*(*sep));
                    minB=std::min(minB,(B.center+B.a[0]*B.alpha[0]*i+B.a[1]*B.alpha[1]*j+B.a[2]*B.alpha[2]*k)*(*sep));
                    maxB=std::max(maxB,(B.center+B.a[0]*B.alpha[0]*i+B.a[1]*B.alpha[1]*j+B.a[2]*B.alpha[2]*k)*(*sep));
                }

        //standard interval intersection test -> if none, we found a separating axes
        if(minA>maxB||minB>maxA)
            return false;

    }

    //none found -> intersecting
    return true;
}

void OBB::draw(Intersect intersect){
    setIntersectColor(intersect);

    glLineWidth(4.0);
    glPushAttrib(GL_LIGHTING);
    glDisable(GL_LIGHTING);

    glPushMatrix();

    glTranslated(center[0],center[1],center[2]);

    glBegin(GL_LINE_LOOP);
    vertex(-1,-1,-1);
    vertex(1,-1,-1);
    vertex(1,1,-1);
    vertex(-1,1,-1);
    glEnd();

    glBegin(GL_LINE_LOOP);
    vertex(-1,-1,1);
    vertex(1,-1,1);
    vertex(1,1,1);
    vertex(-1,1,1);
    glEnd();


    glBegin(GL_LINES);
    vertex(-1,-1,-1);
    vertex(-1,-1,1);

    vertex(1,-1,-1);
    vertex(1,-1,1);

    vertex(1,1,-1);
    vertex(1,1,1);

    vertex(-1,1,-1);
    vertex(-1,1,1);

    vertex(0,0,0);
    vertex(1,0,0);

    vertex(0,0,0);
    vertex(0,1,0);

    vertex(0,0,0);
    vertex(0,0,1);

    glEnd();

    glPopMatrix();

    glPopAttrib();

}

class VectorSortOnAxis{
public:
    VectorSortOnAxis(const std::vector<Vector3d>* P,Vector3d axis):P(P),axis(axis){}

    const std::vector<Vector3d>* P;
    Vector3d axis;

    bool operator() (size_t a, size_t b){
        return (*P)[a]*axis>(*P)[b]*axis;
    }

};

void OBB::splitOBB(const OBB& A, OBB& A1, OBB& A2){
    A.splitOBB(A1,A2);
}

void OBB::splitOBB(OBB& A1, OBB& A2) const{
    //get longest axes
    Vector3d sAxis = a[0]*alpha[0];
    if(sAxis.length()<(a[1]*alpha[1]).length())
        sAxis=a[1]*alpha[1];
    if(sAxis.length()<(a[2]*alpha[2]).length())
        sAxis=a[2]*alpha[2];

    //use a sorted set to sort on the axis
    VectorSortOnAxis sorter(P,sAxis);
    std::set<size_t,VectorSortOnAxis> sorted(sorter);

    for(auto n = indices.begin();n!=indices.end();++n){
        sorted.insert(*n);
    }

    std::set<size_t> indicesA1, indicesA2;

    auto n = sorted.begin();
    for(;indicesA1.size()<((indices.size()+1)/2);++n){
        indicesA1.insert(*n);
    }
    for(;n!=sorted.end();++n){
        indicesA2.insert(*n);
    }

    A1.calc(*P,indicesA1);
    A2.calc(*P,indicesA2);

    return;
}

