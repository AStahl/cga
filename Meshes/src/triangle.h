#ifndef TRIANGLE
#define TRIANGLE

struct Triangle{
    int ind[3]={0,0,0};
    bool intersects=false;

    Triangle(){}

    Triangle(int a, int b, int c){
        ind[0]=a;
        ind[1]=b;
        ind[2]=c;
    }

    int& operator[](int n){return ind[n];}
};
#endif // TRIANGLE

