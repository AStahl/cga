#ifndef MESH_H
#define MESH_H

#include "boxes.h"
#include "BVT.h"
#include <memory>
#include "triangle.h"

class Mesh{
private:
    std::unique_ptr<AABB> aabb=nullptr;
    std::unique_ptr<OBB> obb=nullptr;
    std::unique_ptr<OBB> obbSplit[2]={nullptr,nullptr};
    std::unique_ptr<BVT> bvt=nullptr;

public:
    std::vector<Vector3d> P;// the coords of the loaded model
    std::vector<Triangle> tr;   // the faces of the loaded model,
                // tr[i][0] tr[i][1] tr[i][2]
                // contains the indices of the i-th triangle

    Vector3d& point(int n) {
        return P[tr[n/3][n%3]];
    }

    Vector3d& point(int n,int i) {
        return P[tr[n][i]];
    }

private:
    Vector3d center_;

    Vector3d pos_;
    double zoom_;
    Quat4d rot_;

    void recalcAABB();
public:
    void loadPolyhedron();

    const Vector3d& center() const{return center_;}
    const Vector3d& pos() const{return pos_;}
    const double& zoom() const{return zoom_;}
    const Quat4d& rotation() const{return rot_;}

    void move(const Vector3d mv);
    void rotate(const Quat4d q);
    void scale(const double s);

   // min, max and center of the coords of the loaded model

    void clearIntersect();
    void intersect(Mesh& B);
    void draw(int r, int g, int b, Intersect interAABB, Intersect interOBB, int iR, int iG, int iB);

    int drawDeep=0;
    void drawBall(BVT *tree, int deep=-1);
    void drawBall(int deep=-1){drawBall(bvt.get(),deep);}

    AABB getAABB();
    OBB getOBB();
    BVT* getBVT(){return bvt.get();}
};

bool triangleIntersect(std::vector<Vector3d> pa,std::vector<Vector3d> pb);
#endif // MESH_H
