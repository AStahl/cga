#ifndef BVT_H
#define BVT_H

#include "vecmath.h"
#include <vector>
#include <set>
#include "Sphere.h"


/// Klassische rekursive Datenstruktur!
/** In den Knoten speichern wir die Info (points_, und ball)
		Die Kinder muessen Pointer sein (c++!)
*/
class Mesh;

class BVT
{

	private:

        Mesh* const mesh;
        std::set<int> trInd_;

		/// mass center of point set
		Vector3d mass_center_;

		/// inetria matrix of point set
		Matrix4d inertia_;
	
		/// smallest enclosing sphere of point set	
        Sphere ball_;
	
		BVT * left_;
		BVT * right_;

public:
        BVT(Mesh* const mesh);
        BVT(Mesh* const mesh,
            const std::set<int>& trIndices);

        /// create new node
        void create();

		/// get children
        BVT * left() {return left_;}
        BVT * right() {return right_;}

		/// one recursion step to create left and right child
		void split ();

		/// get sphere
        Sphere& ball() {return ball_;}

		/// anzahl der Punkte
        int nr_of_points () {return trInd_.size()*3;}

        void noIntersect();
        bool intersect(const Sphere& S);
        bool intersect(BVT& B);
	
};






#endif //BVT
