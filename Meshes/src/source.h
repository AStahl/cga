#pragma once
//#ifndef DEMO_H
//#define DEMO_H

#include <QMainWindow>
#include <QGLWidget>
#include <QGridLayout>
#include <QFrame>
#include <vector>
#include <iostream>
#include <algorithm>

#if _MSC_VER
    #include <gl/glu.h>
#elif __APPLE__
  #include <OpenGL/glu.h>
#else
    #include <GL/glu.h>
#endif


#include "vecmath.h"
#include "mesh.h"

class CGView;


class CGMainWindow : public QMainWindow {
	Q_OBJECT

public:

    CGMainWindow (QWidget* parent = 0, Qt::WindowFlags flags = Qt::Window);
	~CGMainWindow ();
    static CGMainWindow* ptr;

    bool showAABB;
    bool collideAABB;
    bool showOBB;
    bool collideOBB;
    bool showSplitOBB;
    bool collideMesh;
    bool showSphere;
    bool showBVT;
    bool collideBVT;

public slots:

	void loadPolyhedron();
    void loadSecondPolyhedron();
    void setShowAABB(bool b);
    void setCollideAABB(bool b);
    void setShowOBB(bool b);
    void setCollideOBB(bool b);
    void setShowSplitOBB(bool b);
    void setCollideMesh(bool b);
    void setShowSphere(bool b);
    void setShowBVT(bool b);
    void setCollideBVT(bool b);

protected:

	void keyPressEvent(QKeyEvent*);

private:

	CGView *ogl;

};

class CGView : public QGLWidget {
	Q_OBJECT

public:

	CGView(CGMainWindow*,QWidget*);
	void initializeGL();

	double zoom;
	int buttonState;
    double phi,theta;
    Vector3d center;

    Mesh obj1;
    Mesh obj2;
    Sphere test_kugel;
	
protected:

	void paintGL();
	void drawMesh();
	void resizeGL(int,int);
	void mouseToTrackball(int x, int y, int W, int H, Vector3d &v);
	Quat4d trackball(const Vector3d&, const Vector3d&);

	void mouseMoveEvent(QMouseEvent*);
	void mousePressEvent(QMouseEvent*);
	void mouseReleaseEvent(QMouseEvent*);
	void wheelEvent(QWheelEvent*);

	CGMainWindow *main;
	Quat4d q_now;
    	int oldX,oldY;

};

//#endif
