#include "PBDObj.h"

PBDObj::PBDObj(){}

PBDObj::PBDObj(std::vector<Vector3d>& x0, std::vector<Vector3d>& v0, std::vector<double> w0, std::vector<distConstraint> constraints, int dimx, int dimy){
    gravity = 9.81;
    solverIterations = 5;
    n = x0.size();
    x = x0;
    v = v0;
    w = w0;
    distConstraints = constraints;
    p.resize(x.size());
    this->dimx = dimx;
    this->dimy = dimy;

    sphereCenter=Vector3d(0,0,0);
    radius=0.2;
}

//Führt einen Diskretisionsschritt durch
void PBDObj::makeStep(double dt){

    for(auto i=0;i<n;++i){
        v[i]+=Vector3d(0,-1,0)*dt*w[i]*gravity;
    }

    //dampvelocities

    p = x;
    for(auto i=0;i<n;++i){
        p[i]+=v[i]*dt;
    }

    //generate CollisionConstraints

    for(int l=0;l<solverIterations;l++){
        projectConstraints();
    }


    for(auto i=0;i<n;++i){
        v[i]=(p[i]-x[i])/dt;
        x[i]=p[i];
    }


}

//Zeichnet das Tuch
void PBDObj::draw(bool wireFrame){
    //glScaled(10,10,10);
    glPointSize(5);

    if(wireFrame){
        glColor3d(0,0,0);
        glBegin(GL_POINTS);
        for(auto i=x.begin();i!=x.end();++i){
            glVertex3dv(i->ptr());
        }
        glEnd();

        glColor3d(0.4,0.4,0.4);
        glBegin(GL_LINES);
        for(auto c=distConstraints.begin();c!=distConstraints.end();++c){
            glVertex3dv(x[c->i].ptr());
            glVertex3dv(x[c->j].ptr());
        }
        glEnd();
    }else{
        if(dimx==1||dimy==1){
            glColor3d(0.6,0.6,0.6);
            glBegin(GL_LINES);

            for(int i=0;i<x.size()-1;++i){
                glVertex3dv(x[i].ptr());
                glVertex3dv(x[i+1].ptr());
            }
            glEnd();
        }else{
            glColor3d(0.6,0.6,0.6);
            glBegin(GL_QUADS);

            Vector3d normal;
            for(int iy=0;iy<dimy-1;++iy)
                for(int ix=0;ix<dimx-1;++ix){
                    normal = (x[(iy+1)*dimx+ix]-x[iy*dimx+ix])%(x[iy*dimx+(ix+1)]-x[iy*dimx+ix]);
                    glNormal3dv(normal.ptr());
                    glVertex3dv(x[iy*dimx+ix].ptr());
                    glVertex3dv(x[(iy+1)*dimx+ix].ptr());
                    glVertex3dv(x[(iy+1)*dimx+(ix+1)].ptr());
                    glVertex3dv(x[iy*dimx+(ix+1)].ptr());
                }
            glEnd();
        }
    }


    glColor3d(0,0,0);

    glPushAttrib(GL_LIGHTING);
    glDisable(GL_LIGHTING);

    GLUquadricObj *quadric;
    quadric = gluNewQuadric();

    glPushMatrix();
    glTranslated(sphereCenter[0],sphereCenter[1],sphereCenter[2]);
    gluQuadricDrawStyle(quadric, GLU_LINE);
    gluSphere( quadric , radius,20,20);
    //glutWireSphere(ball_.radius,50,50);
    glPopMatrix();

    glPopAttrib();
}

void PBDObj::projectConstraints()
{
    Vector3d pd;
    double d;
    for(auto c=distConstraints.begin();c!=distConstraints.end();++c){
        pd=p[c->j]-p[c->i];

        p[c->i]+=pd*(1-(c->d)/pd.length())*w[c->i]/(w[c->i]+w[c->j]);
        p[c->j]-=pd*(1-(c->d)/pd.length())*w[c->j]/(w[c->i]+w[c->j]);
    }

    // "iterate over collision constraints"
    for(auto i=p.begin();i!=p.end();++i){
        d=(*i-sphereCenter).length();
        if(d<radius*1.1)
            *i=sphereCenter+(*i-sphereCenter)*(radius/d*1.1);
    }
}
