#ifndef PBDOBJ_H
#define PBDOBJ_H

#include "vecmath.h"
#ifndef VECMATH_VERSION
#  error "vecmath-library might be out-of-date. Please use newer version"
#endif

#if _MSC_VER
    #include <gl/glu.h>
#elif __APPLE__
  #include <OpenGL/glu.h>
#else
    #include <GL/glu.h>
#endif

#include <QtOpenGL>
#include <iostream>
#include <vector>

class distConstraint{
public:
    int i,j;    //indizes der beiden Partikel
    double d;   //Ruhelänge
    distConstraint(){}
    distConstraint(int i, int j, float d){this->i=i; this->j=j; this->d=d;}
};

class PBDObj
{
public:
    PBDObj();

    PBDObj(std::vector<Vector3d>& x0, std::vector<Vector3d>& v0, std::vector<double> w0,
           std::vector<distConstraint> constraints, int dimx, int dimy);

    void makeStep(double dt);
    void draw(bool wireFrame);

    //Anzhal der Partikel
    int n;
    int solverIterations, dimx, dimy;
	
    //position, vorhergesagte position und geschwindigkeit
    std::vector<Vector3d> x,p,v;

    //inverse Masse
    std::vector<double> w;

    //Erdbeschleunigung
    double gravity;

    //constraints
    std::vector<distConstraint> distConstraints;

    Vector3d sphereCenter;
    double radius;

    protected:
    void projectConstraints();
};
#endif //PBDOBJ_H
