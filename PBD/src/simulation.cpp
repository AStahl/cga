#include <QApplication>
#include <QMenu>
#include <QMenuBar>
#include <QStatusBar>
#include <QFileDialog>
#include <QGLWidget>
#include <QKeyEvent>
#include <QMessageBox>
#include <QHBoxLayout>
#include <QTimer>
#include <QtOpenGL>

#define _USE_MATH_DEFINES
#include <cmath>
#include <fstream>
#include <sstream>
#include <iostream>
#include <limits>

#include "vecmath.h"
#ifndef VECMATH_VERSION
#  error "vecmath-library might be out-of-date. Please use newer version"
#endif

#include <time.h>

#include "simulation.h"

const double infinity = 1e20;

const unsigned int TIMESTEP = 10; // in msec
bool moving = false;

//Die Timer-Funktion, die regelm"assig in TIMESTEP Schritten aufgerufen wird
void CGView::timer()
{
    if (moving == true){
        cloth.makeStep(TIMESTEP/1000.0);
        updateGL();
    }
}

CGMainWindow::CGMainWindow (QWidget* parent, Qt::WindowFlags flags)
: QMainWindow (parent, flags) {
	resize (604, 614);

	// Create a nice frame to put around the OpenGL widget
	QFrame* f = new QFrame (this);
	f->setFrameStyle(QFrame::Sunken | QFrame::Panel);
	f->setLineWidth(2);

	// Create our OpenGL widget
	ogl = new CGView (this,f);

	// Create a menu
	QMenu *file = new QMenu("&File",this);
	file->addAction ("Quit", this, SLOT(close()), Qt::CTRL+Qt::Key_Q);

	menuBar()->addMenu(file);

	// Put the GL widget inside the frame
	QHBoxLayout* layout = new QHBoxLayout();
	layout->addWidget(ogl);
	layout->setMargin(0);
	f->setLayout(layout);

	setCentralWidget(f);

	statusBar()->showMessage("Ready",1000);
}

CGMainWindow::~CGMainWindow () {}

CGView::CGView (CGMainWindow *mainwindow,QWidget* parent ) : QGLWidget (parent), quad(NULL) {
	main = mainwindow;

	/// Um Keyboard-Events durchzulassen
	setFocusPolicy(Qt::StrongFocus);
}

void CGView::initializeGL() {
	glClearColor(1.0,1.0,1.0,1.0);
	q_now = Quat4d(0.0,0.0,0.0,1.0);
    zoom = 1.0;
	
    //glEnable(GL_CULL_FACE);
    //glFrontFace(GL_CCW);
    //glCullFace(GL_BACK);
    glEnable(GL_DEPTH_TEST);

    glLightModeli(GL_LIGHT_MODEL_TWO_SIDE, GL_TRUE);
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_NORMALIZE);
    glEnable(GL_COLOR_MATERIAL);
    glEnable(GL_POINT_SMOOTH);
    glEnable(GL_LINE_SMOOTH);
    glEnable(GL_POLYGON_SMOOTH);
    glHint(GL_LINE_SMOOTH_HINT, GL_NICEST);

    //init Cloth
    dimx = 20;
    dimy = 20;
    initPBDObject();
    moving = false;
    wireFrame = false;

	///Make timer
    QTimer *timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(timer()));
    timer->start(TIMESTEP); /// also 100 mal pro sekunde!
}

void CGView::initPBDObject(){
    std::vector<Vector3d> x0, v0;
    std::vector<double> w0;
    std::vector<distConstraint> constraints;

    x0.resize(dimx*dimy);
    v0.resize(dimx*dimy);
    w0.resize(dimx*dimy);

    double dx=(dimx>1)?2.0/(dimx-1):1;
    double dy=(dimy>1)?2.0/(dimy-1):1;
    double dd = sqrt(dx*dx+dy*dy);

    for(int y=0;y<dimy;++y)
        for(int x=0;x<dimx;++x)
        {
            int ind = y*dimx+x;

            x0[ind]=Vector3d(
                        (dimx>1)?x*dx-1:0,
                        1-(0.1)*y*dy,
                        (0.9)*y*dy
                        );
            v0[ind]=Vector3d(0,0,0);
            w0[ind]=1;

            if(x<dimx-1)
                //rechter Nachbar
                constraints.push_back(distConstraint(ind, ind+1, dx));

            if(y<dimy-1)
                //unterer Nachbar
                constraints.push_back(distConstraint(ind, ind+dimx, dy));

            if(x<dimx-1 && y<dimy-1)
                //rechter unterer Nachbar
                constraints.push_back(distConstraint(ind, ind+dimx+1, dd));

            if(x>0 && y<dimy-1)
                //rechter unterer Nachbar
                constraints.push_back(distConstraint(ind, ind+dimx-1, dd));
        }

    w0[0]=0;
    w0[dimx-1]=0;

    cloth = PBDObj(x0,v0,w0,constraints,dimx,dimy);
}

void CGView::paintGL() {
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	/*	Falls perspektivische Projektion verwendet wird, Szene etwas nach hinten schieben, 
		damit sie in der Sichtpyramide liegt. */
    glTranslated(0.0,0.0,-3.0);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	Matrix4d R(q_now);
	Matrix4d RT = R.transpose();
	glMultMatrixd(RT.ptr());

    glScaled(zoom,zoom,zoom);

    cloth.draw(wireFrame);

    //Zeichne Koordinatensystem
    glDisable(GL_LIGHTING);
    glPushMatrix();
    glBegin(GL_LINES);
        glColor3d(1,0,0);
        glVertex3d(0,0,0);
        glVertex3d(1,0,0);
        glColor3d(0,1,0);
        glVertex3d(0,0,0);
        glVertex3d(0,1,0);
        glColor3d(0,0,1);
        glVertex3d(0,0,0);
        glVertex3d(0,0,1);
    glEnd();
    glPopMatrix();
    glEnable(GL_LIGHTING);
}

void CGView::resizeGL(int width, int height) {
	glViewport(0,0,width,height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	if (width > height) {
		double ratio = width/(double) height;
        gluPerspective(45,ratio,1.0,10000.0);
        //glOrtho(-ratio,ratio,-1.0,1.0,-10.0,10.0);
    }
    else {
		double ratio = height/(double) width;
        gluPerspective(45,1.0/ratio,0.01,10000.0);
        //glOrtho(-1.0,1.0,-ratio,ratio,-10.0,10.0);
    }

	glMatrixMode (GL_MODELVIEW);
}

void CGView::worldCoord(int x, int y, int z, Vector3d &v) {
	GLint viewport[4];
	glGetIntegerv(GL_VIEWPORT,viewport);
	GLdouble M[16], P[16];
	glGetDoublev(GL_PROJECTION_MATRIX,P);
	glGetDoublev(GL_MODELVIEW_MATRIX,M);
	gluUnProject(x,viewport[3]-1-y,z,M,P,viewport,&v[0],&v[1],&v[2]);
}

void CGView::mousePressEvent(QMouseEvent *event) {
	oldX = event->x();
	oldY = event->y();
    update();
}

void CGView::mouseReleaseEvent(QMouseEvent*) {}

void CGView::wheelEvent(QWheelEvent* event) {
    if (event->delta() < 0) zoom *= 1.2; else zoom *= 1/1.2;
    update();
}

void CGView::mouseToTrackball(int x, int y, int W, int H, Vector3d &v) {
	if (W > H) {
		v[0] = (2.0*x-W)/H;
		v[1] = 1.0-y*2.0/H;
	} else {
		v[0] = (2.0*x-W)/W;
		v[1] = (H-2.0*y)/W;
	}
	double d = v[0]*v[0]+v[1]*v[1];
	if (d > 1.0) {
		v[2] = 0.0;
		v /= sqrt(d);
	} else v[2] = sqrt(1.0-d*d);
}

Quat4d CGView::trackball(const Vector3d& u, const Vector3d& v) {
	Vector3d uxv = u % v;
	Quat4d ret(uxv[0],uxv[1],uxv[2],1.0+u*v);
	ret.normalize();
	return ret;
}

void CGView::mouseMoveEvent(QMouseEvent* event) {
	Vector3d p1,p2;

	mouseToTrackball(oldX,oldY,width(),height(),p1);
	mouseToTrackball(event->x(),event->y(),width(),height(),p2);
	
	Quat4d q = trackball(p1,p2);
	q_now = q * q_now;
	q_now.normalize();

	oldX = event->x();
	oldY = event->y();

	updateGL();
}

#define delta 0.02

void CGView::keyPressEvent( QKeyEvent * event) 
{
	switch (event->key()) {
        case Qt::Key_Space : moving = !moving; break;
        case Qt::Key_R     : moving = false; initPBDObject(); break;
        case Qt::Key_F     : wireFrame = !wireFrame; break;
        case Qt::Key_1     : moving = false;
                             if(dimx>1) dimx--;
                             initPBDObject();
                             break;
        case Qt::Key_2     : moving = false;
                             dimx++;
                             initPBDObject();
                             break;
        case Qt::Key_3     : moving = false;
                             if(dimy>1) dimy--;
                             initPBDObject();
                             break;
        case Qt::Key_4     : moving = false;
                             dimy++;
                             initPBDObject();
                             break;
    case Qt::Key_A     : cloth.sphereCenter[0]+=delta; break;
    case Qt::Key_D     : cloth.sphereCenter[0]-=delta; break;
    case Qt::Key_W     : cloth.sphereCenter[1]+=delta; break;
    case Qt::Key_S     : cloth.sphereCenter[1]-=delta; break;
    case Qt::Key_Q     : cloth.sphereCenter[2]+=delta; break;
    case Qt::Key_E     : cloth.sphereCenter[2]-=delta; break;
		default: break;
	}

	updateGL();
}

int main (int argc, char **argv) {
	QApplication app(argc, argv);

	if (!QGLFormat::hasOpenGL()) {
		qWarning ("This system has no OpenGL support. Exiting.");
		return 1;
	}

	CGMainWindow *w = new CGMainWindow(NULL);

	w->show();

	return app.exec();
}

