#ifndef SIMULATION_H
#define SIMULATION_H

#include <QMainWindow>
#include <QGLWidget>
#include <QGridLayout>
#include <QFrame>
#include <QActionGroup>
#include <QMenu>
#include <vector>
#include <iostream>

#if _MSC_VER
    #include <gl/glu.h>
#elif __APPLE__
  #include <OpenGL/glu.h>
#else
    #include <GL/glu.h>
#endif

#include "vecmath.h"
#include "PBDObj.h"

#ifndef VECMATH_VERSION
#error "wrong vecmath included, must contain a VECMATH_VERSION macro"
#else
#if VECMATH_VERSION < 2
#error "outdatet vecmath included"
#endif
#endif

class CGView;

class CGMainWindow : public QMainWindow {
	Q_OBJECT

public:

    CGMainWindow (QWidget* parent = 0, Qt::WindowFlags flags = Qt::Window);
	~CGMainWindow ();

private:

	CGView *ogl;
};

class CGView : public QGLWidget {
	Q_OBJECT

public:

	CGView(CGMainWindow*,QWidget*);
	void initializeGL();
	
	/** transforms the picture coords (x,y,z) to world coords by 
		inverting the projection and modelview matrix (as it it is 
		after invocation of paintGL) and stores the result in v */
	void worldCoord(int x, int y, int z, Vector3d &v);

	double zoom;

protected:
	void paintGL();
	void resizeGL(int,int);
	void mouseToTrackball(int x, int y, int W, int H, Vector3d &v);
	Quat4d trackball(const Vector3d&, const Vector3d&);

	void mouseMoveEvent(QMouseEvent*);
	void mousePressEvent(QMouseEvent*);
	void mouseReleaseEvent(QMouseEvent*);
    void wheelEvent(QWheelEvent*event);
	void keyPressEvent( QKeyEvent * event); 

	CGMainWindow *main;
	int oldX,oldY;
	GLUquadric *quad;
	Quat4d q_now;

    int dimx,dimy;
    bool wireFrame;
    void initPBDObject();
    PBDObj cloth;

public slots:

    void timer ();
};

#endif
