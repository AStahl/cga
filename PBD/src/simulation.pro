TEMPLATE = app
TARGET = simulation 
QT += gui opengl
CONFIG += console
CONFIG += c++11
HEADERS += *.h
SOURCES += *.cpp 

macx: QMAKE_MAC_SDK = macosx10.9
unix:!macx: LIBS+= -lGLU

QMAKE_CXXFLAGS += -std=c++11
