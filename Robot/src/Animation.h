#ifndef ENVMAP_H
#define ENVMAP_H

//#include <GL/glu.h>
#include <QMainWindow>
#include <QGLWidget>
#include <QGridLayout>
#include <QFrame>
#include <QActionGroup>
#include <QMenu>
#include <QGLShader>
#include <QFileInfo>

#include <vector>
#include <iostream>

#include "vecmath.h"

#ifdef near
#undef near
#endif
#ifdef far
#undef far
#endif

#ifndef VECMATH_VERSION
#error "wrong vecmath included, must contain a VECMATH_VERSION macro"
#else
#if VECMATH_VERSION < 2
#error "outdatet vecmath included"
#endif
#endif

#if _MSC_VER
    #include <gl/glu.h>
#elif __APPLE__
  #include <OpenGL/glu.h>
#else
    #include <GL/glu.h>
#endif


class CGView;

class CGMainWindow : public QMainWindow {
	Q_OBJECT

public:

    CGMainWindow (QWidget* parent = 0, Qt::WindowFlags flags = Qt::Window);
	CGView *ogl;

private:


};

class CGView : public QGLWidget {
	Q_OBJECT

public:

	CGView(CGMainWindow*,QWidget*);
	void initializeGL();
	
	double zoom,phi,theta;
	float t;

	void animate_scene(double time);


protected:
	void paintGL();
	void resizeGL(int,int);
	void mouseToTrackball(int x, int y, int W, int H, Vector3d &v);
	Quat4d trackball(const Vector3d&, const Vector3d&);

	void mouseMoveEvent(QMouseEvent*);
	void mouseReleaseEvent(QMouseEvent*);
	void mousePressEvent(QMouseEvent*);
	void wheelEvent(QWheelEvent*);

	CGMainWindow *main;
	int oldX,oldY;
	Quat4d q_now;

	void inc_alpha (double a);
	void inc_beta  (double b);
	void inc_gamma (double c);
	void delta_phi (Vector3d phi);
	Vector3d X (double t);
	Vector3d forward_kinematic();
	void makeMove ();
	void drawX ( int slices);
	void drawRobot ();
	void updateJacobi();
	void updateInvJacobi();
	void keyPressEvent (QKeyEvent * event);

private:
	Matrix4d J,J_inv; 
	// Ursprung
	Vector3d o;//(0,0,STRUT_A);

	/// Die Anfangsparameter der Gelenke stellen sicher, 
	/// dass der Endeffektor zum Zeitpunkt 0 auf der Koordinate
	/// X(0) steht. Dies ist wichtig f"ur die sp"atere Diskretisierung
	/// bei der R"uckw"artskinematik!
	double alpha_;// = 0; 
	double beta_ ;//	= 60; 
	double gamma_;// = 30;

public:
	// Animation Stuff
	double T, dT;
	int MBpressed;


	public slots:
		void timer();


};

#endif
