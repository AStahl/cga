
#include <QtOpenGL>

#include <QApplication>
#include <QMenu>
#include <QMenuBar>
#include <QStatusBar>
#include <QFileDialog>
#include <QGLWidget>
#include <QKeyEvent>
#include <QMessageBox>
#include <QHBoxLayout>

#define _USE_MATH_DEFINES
#include <cmath>
#include <fstream>
#include <sstream>
#include <iostream>
#include <limits>

#include <assert.h>

#include "Animation.h"

#define PI 3.14159265
// Arml"angen des Roboters
#define STRUT_A 1
#define STRUT_B 0.75
#define STRUT_C 0.5

CGMainWindow::CGMainWindow (QWidget* parent, Qt::WindowFlags flags)
	: QMainWindow (parent, flags) {
		resize (604, 614);

		// Create a nice frame to put around the OpenGL widget
		QFrame* f = new QFrame (this);
		f->setFrameStyle(QFrame::Sunken | QFrame::Panel);
		f->setLineWidth(2);

		// Create our OpenGL widget
		ogl = new CGView (this,f);

		// Create a menu
		QMenu *file = new QMenu("&File",this);
		file->addAction ("Quit", this, SLOT(close()), Qt::CTRL+Qt::Key_Q);

		menuBar()->addMenu(file);

		// Put the GL widget inside the frame
		QHBoxLayout* layout = new QHBoxLayout();
		layout->addWidget(ogl);
		layout->setMargin(0);
		f->setLayout(layout);

		setCentralWidget(f);

		statusBar()->showMessage("Ready",1000);
	}

CGView::CGView (CGMainWindow *mainwindow,QWidget* parent ) : QGLWidget (parent), main(mainwindow), t(0) {
	/// Um Keyboard-Events durchzulassen
	setFocusPolicy(Qt::StrongFocus);
}

void CGView::initializeGL() {


	o = Vector3d(0,0,STRUT_A);
	alpha_= 0; 
	beta_ = 60.0 / 180.0 * PI; 
	gamma_= 30.0 / 180.0 * PI;

	glClearColor(0.0,0.0,0.0,0.0);
	zoom = 1.0;
	q_now = Quat4d(1.0,0.0,0.0,-1.0);
	T = 0; dT = 10; // ms
	MBpressed = 0;
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_NORMALIZE);
	glEnable(GL_COLOR_MATERIAL);

	GLfloat mat_specular[] = { 1.0, 1.0, 1.0, 1.0 };
	GLfloat mat_shininess[] = { 1.0 };
	GLfloat light_position[] = { 0.0, 0.0, 1.0, 0.0 };
	glClearColor (0.3, 0.3, 0.3, 0.0);
	glShadeModel (GL_SMOOTH);
	glMaterialfv(GL_FRONT, GL_SPECULAR, mat_specular);
	glMaterialfv(GL_FRONT, GL_SHININESS, mat_shininess);
	glLightfv(GL_LIGHT0, GL_POSITION, light_position);
	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);
	///Make timer
	QTimer *timer = new QTimer(this);
	connect(timer, SIGNAL(timeout()), this, SLOT(timer()));
	timer->start(dT); 
}


void CGView::keyPressEvent(QKeyEvent * event){
	switch(event->key()){
		case Qt::Key_Q:
			inc_alpha(-0.01);
			break;
		case Qt::Key_A:
			inc_alpha(0.01);
			break;
		case Qt::Key_W:
			inc_beta(-0.01);
			break;
		case Qt::Key_S:
			inc_beta(0.01);
			break;
		case Qt::Key_E:
			inc_gamma(-0.01);
			break;
		case Qt::Key_D:
			inc_gamma(0.01);
			break;
			break;
	}
	updateGL();
}


/// inkrementiert die einzelnen Winkel der Roboter-Arme
void CGView::inc_alpha (double a) {alpha_+=a;}
void CGView::inc_beta  (double b) {beta_ +=b;}
void CGView::inc_gamma (double c) {gamma_+=c;}

/// F"uhrt einen Bewegungsschrit des Roboters um Winkel-Vektor phi aus
void CGView::delta_phi (Vector3d phi)
{
	inc_alpha (phi[0]);
	inc_beta  (phi[1]);
	inc_gamma (phi[2]);
}

/// eine h"ubsche zyklische dreidimensionale Funktion, die mit einem Wert
/// t parametrisierbar ist. Ihre Funktionswerte geben die Positionen des 
/// Endeffektors vor! Sie ist so gew"ahlt, dass X(0) der Anfangskonfiguration
/// des Endeffektors entspricht!
Vector3d CGView::X (double t)
{
	double r = STRUT_B*(sqrt(3)/2.0) + STRUT_C;
	double s = .5;

	double offset = STRUT_A;
	double height = STRUT_A + 0.5*STRUT_B - offset* ((sin(t)+sin(4*t))/(2*PI));

	return Vector3d (r*cos (t), s*sin(t), height);
}

Vector3d CGView::forward_kinematic()
{
	const double ca = cos(alpha_);
	const double sa = sin(alpha_);
	const double cb = cos(beta_);
	const double sb = sin(beta_);
    const double cg = cos(gamma_);
    const double sg = sin(gamma_);

	const double cbg = cos(beta_+gamma_);
	const double sbg = sin(beta_+gamma_);

	const double a = STRUT_A;
	const double b = STRUT_B;
	const double c = STRUT_C;

	// ADD YOUR CODE HERE

    return Vector3d(
                b*sb*ca + c*ca*sbg,
                b*sb*sa + c*sa*sbg,
                a + b*cb + c*cbg);
}


/// Macht einen diskretisierten Simulationsschritt
void CGView::makeMove ()
{
    updateInvJacobi();

    //std::cout << X(t-PI/500)-forward_kinematic() << "\n";

	//	/// ADD YOUR CODE HERE!
    Vector3d dx= X(t)-forward_kinematic();
    Vector3d dPhi=J_inv*dx;


	alpha_ += dPhi[0];
	beta_  += dPhi[1];
	gamma_ += dPhi[2];

}

// Malt die Funktion X, die den Positionen des Endeffektors entspricht
void CGView::drawX ( int slices)
{
	double dt = 2*PI / (double) slices;

	double t=0;

	glBegin (GL_LINE_LOOP);
	for (int i=0;i<slices;i++)
	{
		const Vector3d& v = X (t);
		glVertex3dv (v.ptr());
		t +=dt;
	}
	glEnd();

	glColor3f(0,1,0);
	glPointSize(3);
	glBegin(GL_POINTS);
	glVertex3dv(X(this->t).ptr());
	glEnd();

}

// Malt einen Roboter!
void CGView::drawRobot ()
{
	GLUquadric* disk = gluNewQuadric ();
	GLUquadric* cyl1 = gluNewQuadric ();
	GLUquadric* cyl2 = gluNewQuadric ();
	GLUquadric* cyl3 = gluNewQuadric ();
	GLUquadric* sph1 = gluNewQuadric ();
	GLUquadric* sph2 = gluNewQuadric ();
	glPushMatrix ();
	gluCylinder (disk, .15, .15, .05, 10,10);

	double alpha = alpha_ / PI * 180.0;
	double beta  = beta_  / PI * 180.0;
	double gamma = gamma_ / PI * 180.0;

	glRotated (alpha,0,0,1);
	gluCylinder (cyl1, .05, .05, STRUT_A, 10, 10);

	glTranslated (0,0,STRUT_A);

	gluSphere (sph1,.07,10,10);

	glRotated (beta,0,1,0);
	gluCylinder (cyl2, .05, .05, STRUT_B, 10, 10);

	glTranslated (0,0,STRUT_B);

	gluSphere (sph2,.07,10,10);

	glRotated (gamma,0,1,0);
	gluCylinder (cyl3, .05, 0, STRUT_C, 10, 10);

	glPopMatrix ();
}

// Berechnet die Jacobi-Matrix der Trajektorie
void CGView::updateJacobi()
{



	const double ca = cos(alpha_);
	const double sa = sin(alpha_);
	const double cb = cos(beta_);
	const double sb = sin(beta_);
	const double cg = cos(gamma_);
	const double sg = sin(gamma_);

	const double cbg = cos(beta_+gamma_);
	const double sbg = sin(beta_+gamma_);

	const double a = STRUT_A;
	const double b = STRUT_B;
	const double c = STRUT_C;

    //ADD YOUR CODE HERE!

    J.set ( -b*sa*sb-c*sa*sbg,b*ca*cb+c*ca*cbg,c*ca*cbg,0,
            b*ca*sb+c*ca*sbg,b*cb*sa+c*cbg*sa,c*cbg*sa,0,
            0,-b*sb-c*sbg,-c*sbg,0,
			0,0,0,1);
}

// Updatet die Jacobi-Matrix und berechnet ihre Inverse 
void CGView::updateInvJacobi(){
	updateJacobi ();
	if (!J_inv.invert(J)){
		std::cout << "Jacobi singular! exiting" << std::endl;
		exit(1);
	}
}
void CGView::paintGL() {
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	//glTranslated(0.0,0.0,-3.0); // translate scene if you want to use perspective projection

	Matrix4d M, MT;
	M.makeRotate(q_now);      // q_now describes rotation of scene
	MT = M.transpose();
	glMultMatrixd(MT.ptr());
	glScaled(zoom,zoom,zoom);

	// coord-axis
	glDisable(GL_LIGHTING);
	glBegin(GL_LINES);
	glColor3d (1,0,0);
	glVertex3f(0.0,0.0,0.0);
    glVertex3f(1.0,0.0,0.0);
	glColor3d (0,1,0);
	glVertex3f(0.0,0.0,0.0);
    glVertex3f(0.0,1.0,0.0);
	glColor3d (0,0,1);
	glVertex3f(0.0,0.0,0.0);
    glVertex3f(0.0,0.0,1.0);
	glEnd();

	/// Hier malen wir die Funktion, der der Endeffektor sp"ater folgen soll!
	glDisable (GL_LIGHTING);
	glColor3d (1,0,0);
	drawX ( 100);

	// und hier den Robot
	glEnable(GL_LIGHTING);
	glColor3d (0,0,1);
	drawRobot();

}


void CGView::resizeGL(int width, int height) {
	glViewport(0,0,width,height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	if (width > height) {
		double ratio = width/(double) height;
		glOrtho(-ratio,ratio,-1.0,1.0,-10.0,10.0);
	}
	else {
		double ratio = height/(double) width;
		glOrtho(-1.0,1.0,-ratio,ratio,-10.0,10.0);
	}
	glMatrixMode (GL_MODELVIEW);
}

void CGView::mousePressEvent(QMouseEvent *event) {
	oldX = event->x();
	oldY = event->y();
	if( event->button() == Qt::RightButton ){
		MBpressed = 3;
	}
	if( event->button() == Qt::MiddleButton ){ 
		MBpressed = 2;
	}
	if( event->button() == Qt::LeftButton ){  
		MBpressed = 1;
	}


}

void CGView::mouseReleaseEvent(QMouseEvent* event){
	if( event->button() == Qt::RightButton ){
		MBpressed = 0;
	}
}

void CGView::wheelEvent(QWheelEvent* event) {
	if (event->delta() < 0) zoom *= 1.05; else zoom *= 1/1.05;
	updateGL();
}

void CGView::mouseToTrackball(int x, int y, int W, int H, Vector3d &v) {
	if (W > H) {
		v[0] = (2.0*x-W)/H;
		v[1] = 1.0-y*2.0/H;
	} else {
		v[0] = (2.0*x-W)/W;
		v[1] = (H-2.0*y)/W;
	}
	double d = v[0]*v[0]+v[1]*v[1];
	if (d > 1.0) {
		v[2] = 0.0;
		v /= sqrt(d);
	} else v[2] = sqrt(1.0-d*d);
}

Quat4d CGView::trackball(const Vector3d& u, const Vector3d& v) {
	Vector3d uxv = u % v;
	Quat4d ret(uxv[0],uxv[1],uxv[2],1.0+u*v);
	ret.normalize();
	return ret;
}

void CGView::mouseMoveEvent(QMouseEvent* event) {
	Vector3d p1,p2;

	mouseToTrackball(oldX,oldY,width(),height(),p1);
	mouseToTrackball(event->x(),event->y(),width(),height(),p2);

	Quat4d q = trackball(p1,p2);
	if (MBpressed == 1){ // left button pressen, rotate scene
		q_now=q*q_now; 
		q_now.normalize();
	}
	oldX = event->x();
	oldY = event->y();

	updateGL();
}

void glPrintErrors(const char* as_Function)
{
	GLenum lr_Error = GL_NO_ERROR;
	bool lb_ErrorFound = false;
	int li_ErrorCount = 5;
	do
	{
		lr_Error = glGetError();
		if (lr_Error != GL_NO_ERROR)
		{
			lb_ErrorFound = true;
			li_ErrorCount--;
			char* ls_ErrorString = (char*)gluErrorString(lr_Error);
			std::cerr << "OpenGL Error (" << as_Function << "): " << ls_ErrorString << std::endl;
		}
		if (li_ErrorCount == 0)
		{
			std::cerr << "OPENGL :: ERROR Too many errors!" << std::endl;
			break;
		}
	} while (lr_Error != GL_NO_ERROR);
	if (lb_ErrorFound) exit(0);
};


/// timer-slot for animation
void CGView::timer()
{
	t+= PI/500; // in 500 Schritten einmal halb rum
	makeMove();
	updateGL(); 
}


void CGView::animate_scene(double time){
}


int main (int argc, char **argv) {
	QApplication app(argc, argv);

	if (!QGLFormat::hasOpenGL()) {
		qWarning ("This system has no OpenGL support. Exiting.");
		return 1;
	}

	CGMainWindow *w = new CGMainWindow(NULL);
	w->show();
	return app.exec();
}

