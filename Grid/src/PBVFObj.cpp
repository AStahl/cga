#include "PBVFObj.h"
#include "math.h"

extern Vector3d THEULTIMATIVEGRAV;


PBVFObj::PBVFObj(){}

PBVFObj::PBVFObj(std::vector<Vector3d> x0,
                 std::vector<Vector3d> v0,
                 double rho_0, double k, double k_near, double gravity,
                 double radius, Vector3d sphereCenter)
{
    this->gravity = gravity;
    n = x0.size();
    P.resize(n);
    for(int i = 0; i < n; i++){
        P.at(i) = fluidParticle(x0.at(i), v0.at(i) );
    }

    this->sphereCenter = sphereCenter;
    this->radius = radius;
    sphereW = 0.005;
    this->rho_0 = rho_0;
    this->k = k;
    this->k_near = k_near;

    h = 0.05;
    alpha = 0;
    beta = 1;
    clicked = false;
    hashTableSize = 9999;
    cellSize = 0.06;
    wallx0 =0; wallx1=1.5; wally0=0; wally1=1.5;
}

//Führt einen Diskretisionsschritt durch
void PBVFObj::makeStep(double dt){

     for(fluidParticle& p: P)  //update velocity
        p.v += THEULTIMATIVEGRAV*gravity*dt;

    applyViscosity(dt);

    // (explicit euler) prediction
    for(fluidParticle& p: P){
        p.x_prev = p.x;
        p.x += p.v*dt;
    }
    doubleDensityRelaxation(dt);
	
    // apply gravity to sphere
    if(!clicked){
        sphereCenterPrev = sphereCenter;
        sphereVelo += THEULTIMATIVEGRAV*gravity*dt;
        sphereCenter += sphereVelo*dt;
    }

    collideWalls();
    collideSphere();

    // update velocity in PBD fashion
    for(fluidParticle& p: P)
        p.v = (p.x-p.x_prev)/dt;
    if(!clicked) sphereVelo = (sphereCenter - sphereCenterPrev)/dt;

}

void PBVFObj::findNeighborsInCell(int i, double h2, int x, int y, int z){
    std::vector<int>& cell = grid[hash(x,y,z)];
    for (int j=0; j<cell.size(); j++){
        if(i==cell[j])continue;
        double len2 = (P.at(i).x - P.at(cell[j]).x).lengthSquared();
        if(len2 < h2){
            neighbors[i].push_back(cell[j]);
            //neighbors[j].push_back(i);
        }
    }


}

void PBVFObj::findNeighbors(){
    //reset neighbors and grid
    neighbors.resize(n);
    for(int i=0; i<n; i++) neighbors[i].resize(0);

    grid.resize(hashTableSize);
    for(int i=0; i<hashTableSize; i++) grid[i].resize(0);

    double h2= h*h;

    //add particles to hashtable
    for(int i=0; i<n; i++) grid[hash(P[i].x[0]/cellSize,P[i].x[1]/cellSize,P[i].x[2]/cellSize)].push_back(i);

    //find neighbors
    for(int i=0; i<n; i++){
        int x=P[i].x[0]/cellSize;
        int y=P[i].x[1]/cellSize;
        int z=P[i].x[2]/cellSize;

        for(int dx=-1; dx<2;dx++)
            for(int dy=-1; dy<2;dy++)
                for(int dz=-1; dz<2;dz++)
                    findNeighborsInCell(i,h2, x+dx,y+dy,z+dz);
}

}

void PBVFObj::findNeighborsStupid(){
    //reset neighbors
    neighbors.resize(n);
    for(int i=0; i<n; i++) neighbors[i].resize(0);
    double h2= h*h;
    for(int i=0; i<n-1; i++){
        for(int j=i+1; j<n; j++){
            double len2 = (P.at(i).x - P.at(j).x).lengthSquared();
            if(len2 < h2){
                neighbors[i].push_back(j);
                neighbors[j].push_back(i);
            }
        }
    }
}

void PBVFObj::applyViscosity(double dt){

    findNeighbors();
    //findNeighborsStupid();

    for(int i=0;i<n-1;i++)  //forall particle pairs
    for(unsigned int j=0;j<neighbors[i].size();j++) {
        int other = neighbors[i][j];
        if(other < i) continue; //consider pair only once
        Vector3d rij = P[i].x-P[other].x;
        double rijlen = rij.length();
        rij /= rijlen;

        double q = rijlen/h;
        if( q < 1) {
            double u = (P[i].v - P[other].v)*rij;
            if(u>0) {
                Vector3d dvhalf = rij*((alpha*u+beta*u*u)*dt*(1-q));
                P[i].v -= dvhalf;
                P[other].v += dvhalf;
            }
        }
    }
}

void PBVFObj::doubleDensityRelaxation(double dt){

    findNeighbors();
    //findNeighborsStupid();
    for(int i=0;i<n;i++) {//forall particles i
        double rho = 0;
        double rho_near = 0;

        // save values, that have to be calculated just once
        vector<int> Ni; // neighbours of i (with |rij|<h & q<1)
        vector<Vector3d> ridash; // ^rij for all neighbours j
        vector<double> qi; // the |rij|/h for all neighbours j

        for(unsigned int j=0;j<neighbors[i].size();j++) {//forall neighbors
            int other = neighbors[i][j];

            Vector3d rij = P[other].x-P[i].x;
            double rijlen = rij.length();
            double q = rijlen/h;
            if( q < 1) {
                // values have to be used, if q<1
                Ni.push_back(other);
                ridash.push_back(rij*(1.0/rijlen));
                qi.push_back(q);

                rho += (1-q)*(1-q);
                rho_near += (1-q)*(1-q)*(1-q);
            }
        }
        double p = k*(rho-rho_0);
        double p_near = k_near*rho_near;
        Vector3d dx;

        // iterate egain over neighbours
        // (by above saving it is assured that q<1)
        for(unsigned int k=0;k<Ni.size();k++) {
            Vector3d Dhalf = ridash[k]*0.5*dt*dt*( p*(1-qi[k]) + p_near*(1-qi[k])*(1-qi[k]) );
            P[Ni[k]].x += Dhalf;
            dx -= Dhalf;
        }
        P[i].x += dx;
    }
}

void PBVFObj::collideWalls(){
    for(int i = 0; i < n; i++) {
        Vector3d &x = P[i].x;

        if(x[0] < wallx0) x[0] = wallx0;
        if(x[0] > wallx1) x[0] = wallx1;
        if(x[1] < wally0) x[1] = wally0;
        if(x[1] > wally1) x[1] = wally1;
    }

    // generate Collisions for sphere
    Vector3d & s = sphereCenter;
    // x=0 ist Wand
    if(s[0]-radius < wallx0) {
        s[0] = wallx0+radius;
        // sphereVelo = sphereVelo*(-1.0);
        sphereVelo[0] = -0.4*sphereVelo[0];
    }
    // x=2 ist Wand
    if(s[0]+radius > wallx1) {
        s[0] = wallx1-radius;
        // sphereVelo = sphereVelo*(-1.0);
        sphereVelo[0] = -0.4*sphereVelo[0];
    }
    // y=0 ist Wand
    if(s[1]-radius < wally0) {
        s[1] = wally0+radius;
        sphereVelo[1] = -0.2*sphereVelo[1];
    }
    // y=2 ist Wand
    if(s[1]+radius > wally1) {
        s[1] = wally1-radius;
        sphereVelo[1] = -0.2*sphereVelo[1];
    }
}

void PBVFObj::collideSphere(){
    for(int it=0; it<5; it++){
        double dist2 = radius*radius;
        Vector3d dx = Vector3d();
        for(fluidParticle& p: P){
            Vector3d r =p.x-sphereCenter;
            if(r.lengthSquared() < dist2){
                double C = r.length();
                r /= C;
                C -= radius;
                p.x -= r*(C/(1.0+sphereW));
                dx += r*(C*sphereW/(1.0+sphereW));
            }
        }
        sphereCenter += dx;
    }
}

//hashfunktion aus [Teschner 03]
int PBVFObj::hash(int x, int y, int z){
    return std::abs((((x * 73856093) ^ (y * 19349669) ^ (z * 83492791)) % (hashTableSize)));
}


//Zeichne die Partikel
void PBVFObj::draw(bool wireFrame){

    GLUquadricObj *quadric;
    quadric = gluNewQuadric();
    glScaled(20,20,20);

    glPushMatrix();
    glColor3d(1,0,0);
    glTranslated(sphereCenter.x(), sphereCenter.y(), sphereCenter.z());
    gluSphere( quadric , radius-0.01 , 10 , 10);

    glPopMatrix();


    glColor3d(0,0,1);

    if(wireFrame){  //draw points
        glDisable(GL_LIGHTING);
        glPointSize(5);
        glBegin(GL_POINTS);
        for(int i=0; i < n; i++){
            glVertex3dv(P.at(i).x.ptr());
        }
        glEnd();
        glEnable(GL_LIGHTING);
    }else{  //draw spheres
        for(int i=0; i < n; i++){
            glPushMatrix();
            glTranslated(P.at(i).x.x(),P.at(i).x.y(),P.at(i).x.z() );
            gluSphere( quadric , 0.01 , 10 , 10);
            glPopMatrix();
        }
    }
}
