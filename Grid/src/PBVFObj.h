#ifndef PBVFOBJ_H
#define PBVFOBJ_H

#include "vecmath.h"
#ifndef VECMATH_VERSION
#  error "vecmath-library might be out-of-date. Please use newer version"
#endif

#if _MSC_VER
    #include <gl/glu.h>
#elif __APPLE__
  #include <OpenGL/glu.h>
#else
    #include <GL/glu.h>
#endif

#include <QtOpenGL>
#include <iostream>
#include <vector>


using namespace std;

class fluidParticle{
public:
    Vector3d x, x_prev, v;
    fluidParticle(){}
    fluidParticle(const Vector3d x, const Vector3d v){
        this->x = x;
        this->v = v;

    }

};


class PBVFObj
{
public:
    PBVFObj();

    PBVFObj(std::vector<Vector3d> x0, std::vector<Vector3d> v0,
            double rho_0, double k, double k_near, double gravity,
            double radius, Vector3d sphereCenter
            );

    void makeStep(double dt);
    void draw(bool wireFrame);
    void applyViscosity(double dt);
    void doubleDensityRelaxation(double dt);
    void collideWalls();
    void collideSphere();
    void findNeighbors();
    void findNeighborsStupid();
    int hash(int x, int y, int z);  //hashfunktion aus [Teschner 03]

    //Anzhal der Partikel
    int n;

    //position, vorhergesagte position und geschwindigkeit
    std::vector<fluidParticle> P;

    int hashTableSize;  //Anzahl der Buckets in der hashtable
    float cellSize;  //Kantenlänge der Gridzellen
    std::vector<std::vector<int>> grid, neighbors;  //grid und nachbartabelle

    //Erdbeschleunigung
    double gravity;
    double rho_0;
    double k, k_near;
    double h;
    double alpha, beta;
    double wallx0, wallx1, wally0, wally1;

    Vector3d sphereCenter, sphereCenterPrev;
	Vector3d sphereVelo;

    double radius, sphereW;

    bool clicked;

    void findNeighborsInCell(int i, double h2, int x, int y, int z);
protected:


};
#endif //PBVFObj_H
